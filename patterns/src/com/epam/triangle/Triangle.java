package com.epam.triangle;

/**
 * @author Sergey Mikhluk.
 */
public abstract class Triangle {
    protected double sideA;
    protected double sideB;
    protected double sideC;

    public Triangle() {
        this(0.0, 0.0, 0.0);
    }

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    abstract String draw();
    double findSquare(){
        double p = (sideA + sideB + sideC) / 2;
        return Math.sqrt(p * (p - sideA) * (p- sideB) * (p- sideC));
    }



}
