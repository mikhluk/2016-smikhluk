package com.epam.triangle;

/**
 * @author Sergey Mikhluk.
 */
public class BlackTriangle extends Triangle{
    private String color;

    public BlackTriangle(String color) {
        super();
        this.color = color;
    }

    public BlackTriangle() {
        super();
        color = "black";
    }

    public BlackTriangle(double sideA, double sideB, double sideC, String color) {
        super(sideA, sideB, sideC);
        this.color = color;
    }

    @Override
    String draw() {
        return null;
    }
}
