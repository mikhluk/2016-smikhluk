package com.epam.io;

import java.io.IOException;

import static com.epam.io.FileUtils.*;

/**
 * 1. Create an utility application that do a byte-to-byte comparison
 * content of two files. Print the result of comparison in next way:
 * - Result of comparison for files <file_name_1> <file_name_2>:
 * a) Result of application executing (if the differences were not found):
 * - the differences were not found
 * <p>
 * b) Result of application executing (if the differences were found):
 * - file sizes: <file_name_1> - Х Bytes <file_name_2> - Y Bytes
 * The first difference was found on : <byte_number>.
 * Total quantity of the different bytes - <quantity_of_different_bytes>
 * <p>
 * 2. Modify previous task by adding to the application a possibility
 * of comparison files in UNICODE format. For setting of comparison mode
 * use a parameter: /U
 *
 * The application demonstrate the same functionality by two file-working
 * modes:  byte-mode and UNICODE-mode.
 *
 * Attention! Before running an application, please, copy files from
 * the path src.com.epam.io.resources to d:\
 *
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static final String FILE_1 = "d:/_testfile1.txt";
    public static final String FILE_2 = "d:/_testfile2.txt";
    public static final String FILE_3 = "d:/_testfile3.txt";

    public static void main(String[] args) throws IOException{
        /* Expected result:
        Byte-format comparison:
        File 1 = d:/_testfile1.txt, File 2 = d:/_testfile2.txt
            The differences were not found.
        */
        demoByteMode(FILE_1, FILE_2);

        /* Expected result:
        Byte-format comparison:
        File 1 = d:/_testfile1.txt, File 2 = d:/_testfile3.txt
            Size of file 1: <22> bytes.
            Size of file 2: <24> bytes.
            The first differences was found at: <0> byte.
            Total quantity of different bytes: <3>.
        */
        demoByteMode(FILE_1, FILE_3);

        /* Expected result:
        UNICODE-format comparison:
        File 1 = d:/_testfile1.txt, File 2 = d:/_testfile2.txt
            The differences were not found.
        */
        demoUnicodeMode(FILE_1, FILE_2);

        /* Expected result:
        UNICODE-format comparison:
        File 1 = d:/_testfile1.txt, File 2 = d:/_testfile3.txt
            Size of file 1: <22> bytes.
            Size of file 2: <24> bytes.
            The first differences was found at: <0> byte.
            Total quantity of different bytes: <3>.
        */
        demoUnicodeMode(FILE_1, FILE_3);
    }

    private static void demoByteMode(final String file1, final String file2) throws IOException{
        System.out.println("\nByte-format comparison:");
        System.out.println("File 1 = " + file1 + ", File 2 = " + file2);
        printResult(FileUtils.getFirstDifference(file1, file2, null), file1, file2, null);
    }

    private static void demoUnicodeMode(final String file1, final String file2) throws IOException{
        System.out.println("\nUNICODE-format comparison:");
        System.out.println("File 1 = " + file1 + ", File 2 = " + file2);
        printResult(FileUtils.getFirstDifference(file1, file2, UNICODE_MODE), file1, file2, UNICODE_MODE);
    }

    private static void printResult(final long firstDifference, final String file1, final String file2, final String mode) throws IOException{
        if (firstDifference < 0) {
            System.out.println("    The differences were not found.");
        } else {
            System.out.println("    Size of file 1: <" + FileUtils.sizeOfFile(file1) + "> bytes.");
            System.out.println("    Size of file 2: <" + FileUtils.sizeOfFile(file2) + "> bytes.");
            System.out.println("    The first differences was found at: <" + firstDifference + "> byte.");
            System.out.println("    Total quantity of different bytes: <" + FileUtils.getTotalDifferences(file1, file2, mode) + ">.");
        }
    }
}