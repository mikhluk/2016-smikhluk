package com.epam.io;

import java.io.*;

/**
 * 1. Create an utility application that do a byte-to-byte comparison
 * content of two files. Print the result of comparison in next way:
 * - Result of comparison for files <file_name_1> <file_name_2>:
 * a) Result of application executing (if the differences were not found):
 * - the differences were not found
 * <p>
 * b) Result of application executing (if the differences were found):
 * - file sizes: <file_name_1> - Х Bytes <file_name_2> - Y Bytes
 * The first difference was found on : <byte_number>.
 * Total quantity of the different bytes - <quantity_of_different_bytes>
 * <p>
 * 2. Modify previous task by adding to the application a possibility
 * of comparison files in UNICODE format. For setting of comparison mode
 * use a parameter: /U
 *
 * The application demonstrate the same functionality by two file-working
 * modes:  byte-mode and UNICODE-mode.
 *
 * Attention! Before running an application, please, copy files from
 * the path src.com.epam.io.resources to d:\
 *
 * @author Sergey Mikhluk.
 */
public class FileUtils {
    private enum Differences {FIRST, TOTAL}

    public static final String UNICODE_MODE = "/U";

    public static long getTotalDifferences(final String file1, final String file2, final String mode)
            throws IOException, NullPointerException {
        return UNICODE_MODE.equals(mode) ?
                getDifferencesUnicodeMode(new File(file1), new File(file2), Differences.TOTAL) :
                getDifferencesByteMode(new File(file1), new File(file2), Differences.TOTAL);
    }

    public static long getFirstDifference(final String file1, final String file2, final String mode)
            throws IOException, NullPointerException {
        return UNICODE_MODE.equals(mode) ?
                getDifferencesUnicodeMode(new File(file1), new File(file2), Differences.FIRST) :
                getDifferencesByteMode(new File(file1), new File(file2), Differences.FIRST);
    }

    private static long getDifferencesByteMode(final File file1, final File file2, final Differences firstOrTotal)
            throws IOException {
        long totalDifferences = 0;
        long firstDifference = -1;

        try (BufferedInputStream buffer1 = new BufferedInputStream(new FileInputStream(file1));
             BufferedInputStream buffer2 = new BufferedInputStream(new FileInputStream(file2))) {
            for (long position = 0; position < Math.max(file1.length(), file2.length()); position++) {
                if (buffer1.read() != buffer2.read()) {
                    if (totalDifferences == 0) {
                        firstDifference = position;
                        if (firstOrTotal == Differences.FIRST) {
                            break;
                        }
                    }
                    totalDifferences++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return firstOrTotal == Differences.FIRST ? firstDifference : totalDifferences;
    }

    private static long getDifferencesUnicodeMode(final File file1, final File file2, final Differences firstOrTotal)
            throws IOException {
        long totalDifferences = 0;
        long firstDifference = -1;

        try (BufferedReader buffer1 = new BufferedReader(new FileReader(file1));
             BufferedReader buffer2 = new BufferedReader(new FileReader(file2))) {
            for (long position = 0; position < Math.max(file1.length(), file2.length()); position++) {
                if (buffer1.read() != buffer2.read()) {
                    if (totalDifferences == 0) {
                        firstDifference = position;
                        if (firstOrTotal == Differences.FIRST) {
                            break;
                        }
                    }
                    totalDifferences++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return firstOrTotal == Differences.FIRST ? firstDifference : totalDifferences;
    }

    public static long sizeOfFile(final String file) {
        if (file == null) {
            throw new NullPointerException();
        }
        return new File(file).length();
    }
}