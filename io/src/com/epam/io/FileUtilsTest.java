package com.epam.io;

import org.junit.Test;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.epam.io.FileUtils.UNICODE_MODE;
import static org.junit.Assert.assertEquals;

/**
 * @author Sergey Mikhluk.
 */
public class FileUtilsTest {
    public static final Level LEVEL = Level.INFO;
    private static Logger logger = Logger.getLogger(FileUtilsTest.class.getName());

    public static final String FILE_1 = "d:/_testfile1.txt";
    public static final String FILE_2 = "d:/_testfile2.txt";
    public static final String FILE_3 = "d:/_testfile3.txt";

    @Test
    public void getDifferencesTest1() throws IOException {
        logger.log(LEVEL, "getDifferencesTest1() invoked");

        long expectedFirst = -1L;
        long actualFirst = FileUtils.getFirstDifference(FILE_1, FILE_2, null);
        assertEquals("mismatch at expected first difference",
                expectedFirst, actualFirst);

        long expectedDifferences = 0L;
        long actualDifferences = FileUtils.getTotalDifferences(FILE_1, FILE_2, null);
        assertEquals("mismatch at expected total differences",
                expectedDifferences, actualDifferences);

        logger.log(LEVEL, "getDifferencesTest1() exit");
    }

    @Test
    public void getDifferencesTest2() throws IOException {
        logger.log(LEVEL, "getDifferencesTest2() invoked");

        long expectedFirst = 0L;
        long actualFirst = FileUtils.getFirstDifference(FILE_1, FILE_3, null);
        assertEquals("mismatch at expected first difference",
                expectedFirst, actualFirst);

        long expectedDifferences = 3L;
        long actualDifferences = FileUtils.getTotalDifferences(FILE_1, FILE_3, null);
        assertEquals("mismatch at expected total differences",
                expectedDifferences, actualDifferences);

        logger.log(LEVEL, "getDifferencesTest2() exit");
    }

    @Test
    public void getDifferencesUnicodeTest1() throws IOException {
        logger.log(LEVEL, "getDifferencesUnicodeTest1() invoked");

        long expectedFirst = -1L;
        long actualFirst = FileUtils.getFirstDifference(FILE_1, FILE_2, UNICODE_MODE);
        assertEquals("mismatch at expected first difference",
                expectedFirst, actualFirst);

        long expectedDifferences = 0L;
        long actualDifferences = FileUtils.getTotalDifferences(FILE_1, FILE_2, UNICODE_MODE);
        assertEquals("mismatch at expected total differences",
                expectedDifferences, actualDifferences);

        logger.log(LEVEL, "getDifferencesUnicodeTest1() exit");
    }

    @Test
    public void getDifferencesUnicodeTest2() throws IOException {
        logger.log(LEVEL, "getDifferencesUnicodeTest2() invoked");

        long expectedFirst = -0L;
        long actualFirst = FileUtils.getFirstDifference(FILE_1, FILE_3, UNICODE_MODE);
        assertEquals("mismatch at expected first difference",
                expectedFirst, actualFirst);

        long expectedDifferences = 3L;
        long actualDifferences = FileUtils.getTotalDifferences(FILE_1, FILE_3, UNICODE_MODE);
        assertEquals("mismatch at expected total differences",
                expectedDifferences, actualDifferences);

        logger.log(LEVEL, "getDifferencesUnicodeTest2() exit");
    }
}