package com.epam.threads.try1;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {
    public final static int[] ARRIVAL_INTERVALS = {0, 50, 50, 50, 50, 50, 400, 2500, 50, 50, 50, 50, 50, 2500, 400, 50};
    public static final int WAITING_ROOM_SIZE = 3;

    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(WAITING_ROOM_SIZE);


        BlockingQueue queue = new ArrayBlockingQueue(1);

        Barber barber = new Barber(queue);
        Thread barberThread = new Thread(barber);
        barberThread.start();
        //new Thread(barber).isInterrupted();

        for (int i = 0; i < ARRIVAL_INTERVALS.length; i++) {
            Thread.sleep(ARRIVAL_INTERVALS[i]);

            new Thread(new WaitingRoom(semaphore, queue, barberThread, i+1)).start();

        }

    }
}
