package com.epam.threads.try1;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Barber implements Runnable {
    private BlockingQueue queue;

    public Barber(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        String client;
        while (true) {
            try {
                client = (String) queue.take();
                if (client != null){
                    shave();
                    System.out.println("-->" + client + " подстригся");
                    //System.out.println("-->" + queue.take() + " подстригся");
                } else  {
                    System.out.println("парикмахер спит..");
                    Thread.sleep(500);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private void shave() {
            double d = 0;
            for (int i = 0; i < 5_000_000; i++) {  // just waiting
                d = Math.sin(d);
            }
    }
}
