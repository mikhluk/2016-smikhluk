package com.epam.threads.try1;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class WaitingRoom implements Runnable {
    Semaphore semaphore;
    Thread barberThread;

    BlockingQueue bq;
    String clientId;

    public WaitingRoom(Semaphore semaphore, BlockingQueue bq, Thread barberThread, int clientId) {
        this.semaphore = semaphore;
        this.barberThread = barberThread;

        this.bq = bq;
        this.clientId = "client " + clientId;
    }

    public void run() {
        try {
            System.out.println(clientId + " подошел к двери в " + getTime() + " ms");
            if (semaphore.tryAcquire()) {

                System.out.println(clientId + " зашел в зал");

                System.out.println(barberThread.getState());
                if (barberThread.getState() == Thread.State.WAITING){
                    System.out.println("Будим парикмахера");
                }

                bq.put(clientId);

                System.out.println("<--" + clientId + " ушел (подстригаться) в " + getTime() + " ms");
                semaphore.release();

            } else {
                System.out.println(clientId + " ушел (нет свободных мест)");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getTime() {
        return System.currentTimeMillis() % 1000;
    }
}
