package com.epam.threads.try2;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class WaitingRoom implements Runnable{
    Semaphore semaphore;
    BlockingQueue bq;
    String clientId;

    public WaitingRoom(Semaphore semaphore, BlockingQueue bq, int clientId) {
        this.semaphore = semaphore;
        this.bq = bq;
        this.clientId = "Client" + clientId;
    }

    public void run(){
        try {
            System.out.println(clientId + " подошел к двери в " + getTime() +" ms");
            if (semaphore.tryAcquire()){

                System.out.println(clientId + " зашел в зал");
                System.out.println("<--"+ clientId + " ушел (подстригаться) в " + getTime() +" ms");
                bq.put(clientId);
                semaphore.release();
            } else {
                System.out.println(clientId + " ушел (нет свободных мест)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private long getTime() {
        return System.currentTimeMillis() % 1000;
    }
}
