package com.epam.threads.try2;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Barber implements Runnable {
    private BlockingQueue queue;

    public Barber(BlockingQueue queue) {
        this.queue = queue;

    }

    public void run() {
        while (true) {
            try {


                System.out.println("Парикмахер проверяет есть ли кто в зале. если нету, то спит");
                Thread.sleep(100);   // Парикмахер проверяет есть ли кто в зале, если нету то засыпает
                // todo не нужно ли прибить одноразовые потоки после их отработки?

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            shave();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void shave() throws InterruptedException {
        Thread.sleep(300);
        System.out.println("-->" +queue.take() + " подстригся");
    }
}
