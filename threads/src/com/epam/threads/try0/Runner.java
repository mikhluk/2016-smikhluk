package com.epam.threads.try0;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {
    public final static int[] ARRIVAL_INTERVALS = {0, 50, 50, 50, 50, 50, 2500, 400, 50, 50, 50, 50, 50, 400, 400, 50};
    public static final int WAITING_ROOM_SIZE = 3;

    public static void main(String[] args) throws InterruptedException {
        Semaphore waitingRoomSemaphore = new Semaphore(WAITING_ROOM_SIZE);
        //Semaphore hairRoomSemaphore = new Semaphore(1, true);

        BlockingQueue bq = new ArrayBlockingQueue(1);

        Barber barber = new Barber(bq);
        new Thread(barber).start();

        for (int i = 0; i < ARRIVAL_INTERVALS.length; i++) {
            Thread.sleep(ARRIVAL_INTERVALS[i]);
            new Thread(new WaitingRoom(waitingRoomSemaphore, bq, "Client " + (i+1))).start();
        }  // todo сделать одним потоком а не 30 потоков

        // todo попробовать сделать без семафора , двумя очередями

    }
}
