package com.epam.threads.try0;

import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class HairRoomOld implements Runnable {
    Semaphore hairRoomSemaphore;
    String clientId;

    public HairRoomOld(Semaphore hairRoomSemaphore, String clientId) {
        this.clientId = clientId;
        this.hairRoomSemaphore = hairRoomSemaphore;
    }

    public void run() {
        try {
            System.out.println(clientId + " подошел к залу стрижки в " + getTime() + " ms");
            hairRoomSemaphore.acquire();

            System.out.println("    " + this.clientId + " стрижется");
            Thread.sleep(300);

            System.out.println(clientId + " подстригся в " + getTime() + " ms");
            hairRoomSemaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private long getTime() {   // todo убрать дубли
        return System.currentTimeMillis() % 1000;
    }

}
