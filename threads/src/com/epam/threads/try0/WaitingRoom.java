package com.epam.threads.try0;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * @author Sergey Mikhluk.
 */
public class WaitingRoom implements Runnable{
    Semaphore waitingRoomSemaphore;
    BlockingQueue bq;
    String clientId;

    public WaitingRoom(Semaphore waitingRoomSemaphore, BlockingQueue bq, String clientId) {
        this.waitingRoomSemaphore = waitingRoomSemaphore;
        this.bq = bq;
        this.clientId = clientId;
    }

    public void run(){
        try {
            System.out.println(clientId + " подошел к двери в " + getTime() +" ms");
            if (waitingRoomSemaphore.tryAcquire()){
                System.out.println(clientId + " зашел в зал");

                bq.put(clientId);
                System.out.println("<--" + clientId + " ушел (подстригаться) в " + getTime() + " ms");

                waitingRoomSemaphore.release();
            } else {
                System.out.println(clientId + " ушел (нет свободных мест)");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private long getTime() {
        return System.currentTimeMillis() % 1000;
    }
}
