package com.epam.threads.try0;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Barber implements Runnable {
    private BlockingQueue queue;

    public Barber(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        while (true) {
            System.out.println("есть очередь? " + !queue.isEmpty());
            if (!queue.isEmpty()) {

                try {
                    shave();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                sleep();
            }

        }


//        for (int i = 0; i < 30; i++) {
//            System.out.println("t1: " +i);
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                System.out.println("interrupted");
//                //return;
//            }
//        }


    }

    private void sleep() { //todo
        try {
            System.out.println("    Парикмахер спит...");
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void shave() throws InterruptedException {

        Thread.sleep(300);
        System.out.println("-->" + queue.take() + " подстригли");

    }
}
