package com.epam.threads.try3;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Producer implements Runnable{

    private BlockingQueue<String> queue;

    public Producer(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        int delta = 0;
        for (int i = 0; i < 15; i++) {
            delta = (int) (Math.random()*500);
            System.out.println("Клиент " + i + " пришел через " + delta + " ms, свободных мест в зале " + queue.remainingCapacity());

            System.out.println("    "+(queue.offer("Client " + i)? "--->заходит в зал": "нет мест - уходит"));

            try {
                Thread.sleep(delta);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
