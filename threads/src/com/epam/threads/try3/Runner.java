package com.epam.threads.try3;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {

        BlockingQueue queue = new ArrayBlockingQueue(5);
        Thread producer = new Thread(new Producer(queue));
        Thread consumer = new Thread(new Consumer(queue));

        producer.start();
        consumer.start();
    }
}
