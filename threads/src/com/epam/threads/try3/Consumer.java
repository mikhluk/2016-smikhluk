package com.epam.threads.try3;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Consumer implements Runnable {

    private BlockingQueue queue;

    public Consumer(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {

        while (queue.remainingCapacity() > 0) {
//            System.out.println("Queue size: " + queue.size() +
//                    ", remaining capacity: " + queue.remainingCapacity());

            try {
                Thread.sleep(500);

                System.out.println("    <---подстрили " + queue.take() + ", мест в зале " + queue.remainingCapacity());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}