package com.epam.threads.try4;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class WaitingRoomFlow implements Runnable {

    private BlockingQueue waitingRoomQueue;
    private BlockingQueue barberQueue;
    private Alarm alarm;

    public WaitingRoomFlow(BlockingQueue waitingRoomQueue, BlockingQueue barberQueue, Alarm alarm) {
        this.waitingRoomQueue = waitingRoomQueue;
        this.barberQueue = barberQueue;
        this.alarm = alarm;
    }

    @Override
    public void run() {
        String client;

        while (true) {
            try {
                client = (String) waitingRoomQueue.peek();
                if (client != null) {

                    // пытаемся закинуть в очередь в зал парикмахера, т.к. put, то если там занято - блокируемся
                    barberQueue.put(client);

                    synchronized (alarm) {
                        if (!alarm.isAwake()) {

                            System.out.println("будим парикмахера!!!!");
                            alarm.setAwake(true);
                            alarm.notify();
                        }
                    }

                    // закинули в очередь в зал парикмахера, освобождаем место в зале ожидания
                    waitingRoomQueue.take();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}