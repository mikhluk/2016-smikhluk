package com.epam.threads.try4;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class ClientFlow implements Runnable {

    public static final int CLIENTS_INTERVAL = 1200;
    public static final int LAST_CLIENT = 15;
    private BlockingQueue<String> queue;

    public ClientFlow(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        int interval;
        for (int i = 0; i < LAST_CLIENT; i++) {
            interval = (int) (Math.random() * CLIENTS_INTERVAL);
            System.out.println("Клиент " + i + " пришел через " + interval + " ms, свободных мест в зале " + queue.remainingCapacity());

            System.out.println("    " + (queue.offer("Client " + i) ?
                    "--->Клиент " + i + " заходит в зал ожидания"
                    : "^^^Клиент " + i + " нет мест - уходит"));

            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
