package com.epam.threads.try4;

import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class BarberFlow implements Runnable {

    public static final int CUTTING_DURATION = 600;
    private BlockingQueue queue;
    private Alarm alarm;

    public BarberFlow(BlockingQueue queue, Alarm alarm) {
        this.queue = queue;
        this.alarm = alarm;
    }

    @Override
    public void run() {
        String client;
        while (true) {
            try {
                //client = (String) queue.take();
                client = (String) queue.peek();
                if (client!=null){
                    System.out.println("        " + client + " начали стрижку");
                    Thread.sleep(CUTTING_DURATION);
                    queue.take();

                    System.out.println("<---" + client + " подстрили ");

                } else {
                    System.out.println("...парикмахер засыпает");
                    synchronized (alarm){
                        alarm.setAwake(false);
                        alarm.wait();
                    }
                    //Thread.sleep(10);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}