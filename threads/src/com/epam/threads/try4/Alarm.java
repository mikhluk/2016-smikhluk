package com.epam.threads.try4;

/**
 * @author Sergey Mikhluk.
 */
public class Alarm {
    private static volatile boolean awake = true;

    public boolean isAwake() {
        return awake;
    }

    public synchronized void setAwake(boolean awake) {
        this.awake = awake;
    }
}
