package com.epam.threads.try4;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {
        System.out.println("Среднее время прихода клиентов: " + ClientFlow.CLIENTS_INTERVAL +
        " ms, длительность стрижки = " + BarberFlow.CUTTING_DURATION+ " ms.");

        Alarm alarm = new Alarm();

        BlockingQueue waitingRoomQueue = new ArrayBlockingQueue(3);
        BlockingQueue barberQueue = new ArrayBlockingQueue(1);

        Thread producer = new Thread(new ClientFlow(waitingRoomQueue));
        Thread waitingRoom = new Thread(new WaitingRoomFlow(waitingRoomQueue, barberQueue, alarm));
        Thread consumer = new Thread(new BarberFlow(barberQueue, alarm));

        producer.start();
        waitingRoom.start();
        consumer.start();
    }
}
