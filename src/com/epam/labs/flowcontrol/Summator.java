//package com.epam.labs.p02;
//
///**
// * @author Sergey Mikhluk.
// */
//public class Summator {
//    public static Matrix sum(Matrix matrixA, Matrix matrixB) {
//        if (matrixA == null) throw new NullPointerException();
//        if (matrixB == null) throw new NullPointerException();
//
//        int horizontalSize = matrixA.getHorizontalSize();
//        int verticalSize = matrixA.getVerticalSize();
//
//        if (horizontalSize != matrixB.getHorizontalSize()) throw new IllegalArgumentException();
//        if (verticalSize != matrixB.getVerticalSize()) throw new IllegalArgumentException();
//
//        int sum;
//        Matrix sumMatrix = new Matrix(verticalSize, horizontalSize);
//
//        for (int x = 0; x < verticalSize; x++) {
//            for (int y = 0; y < horizontalSize; y++) {
//                sum = matrixA.getElement(x, y) + matrixB.getElement(x, y);
//                sumMatrix.setElement(x, y, sum);
//            }
//        }
//
//        return sumMatrix;
//    }
//}


package com.epam.labs.flowcontrol;

public class Summator {
    public static Matrix sum(Matrix matrixA, Matrix matrixB) {
        if (matrixA == null || matrixB == null) {
            throw new NullPointerException();
        }

        int verticalSize = matrixA.getVerticalSize();
        int horizontalSize = matrixA.getHorizontalSize();

        if (verticalSize != matrixB.getVerticalSize() || horizontalSize != matrixB.getHorizontalSize()) {
            throw new IllegalArgumentException();
        }

        Matrix matrixSum = new Matrix(verticalSize, horizontalSize);
        int sum;
        for (int i = 0; i < verticalSize; i++) {
            for (int j = 0; j < horizontalSize; j++) {
                sum = matrixA.getElement(i, j) + matrixB.getElement(i, j);
                matrixSum.setElement(i, j, sum);
            }
        }
        return matrixSum;
    }
}