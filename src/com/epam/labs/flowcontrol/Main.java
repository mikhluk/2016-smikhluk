package com.epam.labs.flowcontrol;

public class Main {
    public static void main(String[] args) {
        int i = 3;
        int j = 4;

        Matrix matrixA = null;
        Matrix matrixB = null;
        Matrix matrixSum = null;

        try {
            matrixA = MatrixFactory.createMatrix(i, j);
            matrixB = MatrixFactory.createMatrix(i, j);
            matrixSum = Summator.sum(matrixA, matrixB);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(matrixA);
        System.out.println(matrixB);
        System.out.println(matrixSum);
    }
}
