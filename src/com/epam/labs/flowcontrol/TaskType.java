package com.epam.labs.flowcontrol;

/**
 * @author Sergey Mikhluk.
 */
public class TaskType {

    //public static final long A = 09;

    public static final String str = "0,3";
    public static final double aDouble = 1.2345e+04;
    public static final double INT = .5;

    public static void main(String[] args) {
        System.out.println(20/6);
        System.out.println(20%6);

        System.out.println(-20/6);
        System.out.println(-20%6);

        System.out.println(20/-6);
        System.out.println(20%-6);

        System.out.println(-20/-6);
        System.out.println(-20%-6);

        int i = 10;
        i = -i;
        byte a = 120;
        a*=2;
        System.out.println(a);


        byte b = 127;
        b++;
        System.out.println(b);

        float f1 = 234.234234f;
        float f2 = 2435434.233454234f;
        float f3 = f1 / f2;

        System.out.println(f3);

        long c = 100L;
        float x = 1.f;
        float y = c +x;
        System.out.println(x);

    }
}
