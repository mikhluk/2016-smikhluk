package com.epam.labs.flowcontrol;

public class MatrixFactory {
    private MatrixFactory(){
    }

    public static Matrix createMatrix(int i, int j) {
        Matrix  matrix = new Matrix(i, j);

        for (int x = 0; x < i; x++){
            for (int y = 0; y < j; y++){
                matrix.setElement(x, y, (int) (Math.random() * i * j));

            }
        }
        return matrix;
    }
}
