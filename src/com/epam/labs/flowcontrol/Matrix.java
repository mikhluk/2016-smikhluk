package com.epam.labs.flowcontrol;

public class Matrix {
    private int[][] array;

    public Matrix(int i, int j){
        array = new int[i][j];
    }

    public int getElement(int i, int j) throws ArrayIndexOutOfBoundsException {
        return array[i][j];
    }

    public void setElement(int i, int j, int value) throws ArrayIndexOutOfBoundsException {
        array[i][j] = value;
    }

    public int getVerticalSize(){
        return array.length;
    }

    public int getHorizontalSize(){
        return array[0].length;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(getVerticalSize() * getHorizontalSize());
        buffer.append("{");
        for(int[] tempArray : array){
            buffer.append("{");
            for(int value : tempArray){
                buffer.append(value);
                buffer.append(" ");
            }
            buffer.append("}\n");
        }
        buffer.delete(buffer.length()-1, buffer.length());
        buffer.append("}\n");
        return buffer.toString();
    }
}
