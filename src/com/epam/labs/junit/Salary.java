package com.epam.labs.junit;

/**
 * http://stackoverflow.com/questions/4757800/configuring-intellij-idea-for-unit-testing-with-junit
 *
 * @author Sergey Mikhluk.
 */
public class Salary {
    public int calculateSalary(int salary, int bonus){

        if (salary < 0 || bonus < 0) {
            throw new IllegalArgumentException("Salary or bonus less then 0!");
        }

        return salary + bonus;
    }
}
