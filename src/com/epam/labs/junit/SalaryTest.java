package com.epam.labs.junit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * @author Sergey Mikhluk.
 */
public class SalaryTest {
    public static final Level LEVEL = Level.INFO;
    private static Logger logger = Logger.getLogger(SalaryTest.class.getName());
    private static Salary salary;

    @BeforeClass
    public static void init() {
        logger.log(LEVEL, "----------------------------------------------------------");
        logger.log(LEVEL, "init() invoked");

        salary = new Salary();
        logger.log(LEVEL, "init() exit\n");
    }

    @AfterClass
    public static void tearDown() {
        logger.log(LEVEL, "tearDown() invoked");
        salary = null;

        logger.log(LEVEL, "tearDown() exit");
        logger.log(LEVEL, "----------------------------------------------------------");
    }

    @Test
    public void calculateSalary() {
        logger.log(LEVEL, "calculateSalary() invoked");
        int expected = 5;
        assertEquals(expected, salary.calculateSalary(2, 3));
        logger.log(LEVEL, "calculateSalary() exit");
    }

    @Test(expected = IllegalArgumentException.class)
    public void calculateSalaryNegative() {
        logger.log(LEVEL, "calculateSalaryNegative() invoked");
        salary.calculateSalary(2, -3);
        logger.log(LEVEL, "calculateSalaryNegative() exit");
    }
}