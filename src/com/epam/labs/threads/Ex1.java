package com.epam.labs.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * Cкорость сравнения отработки коллекция CopyOnWrite быстрее (но у меня не быстрее)
 *
 * @author Sergey Mikhluk.
 */
public class Ex1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Integer> list1 = Collections.synchronizedList(new ArrayList<>());
        List<Integer> list2 = new CopyOnWriteArrayList<>();

        fillList(list1, 10000);
        fillList(list2, 10000);

        System.out.println("List synchronized:");
        checkList(list1);
        System.out.println("CopyOnWriteArrayList:");
        checkList(list2);
    }

    private static void checkList(List<Integer> list) throws ExecutionException, InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        ExecutorService ex = Executors.newFixedThreadPool(2);
        Future<Long> f1 = ex.submit(new GetThread(0, 500, list, latch));
        Future<Long> f2 = ex.submit(new GetThread(500, 1000, list, latch));
        latch.countDown();

        System.out.println("Thread 1:" + f1.get() / 1000);
        System.out.println("Thread 2:" + f2.get() / 1000);
    }

    static void fillList(List<Integer> list, int size) {
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
    }

    static class GetThread implements Callable<Long> {
        int start;
        int end;
        List<Integer> list;
        CountDownLatch latch;

        public Long call() throws Exception {
            latch.await();
            long startTime = System.nanoTime();
            for (int i = start; i < end; i++) {
                list.get(i);
            }

            return System.nanoTime() - startTime;
        }

        public GetThread(int start, int end, List<Integer> list, CountDownLatch latch) {
            this.start = start;
            this.end = end;
            this.list = list;
            this.latch = latch;
        }
    }
}