package com.epam.labs.exception.task2;

import java.io.IOException;

/**
 * 2) Define the class that generates an exception in one
 * method and catch it in an other method. Output to the console
 * a string representation of the object caught
 * exception. Add a block "finally" and print
 * from it message that the object of exception was caught.
 *
 * @author Sergey Mikhluk.
 */
public class Main2 {
    public static void main(String[] args) {
        demoCatch1();
        demoCatch2();
    }

    public static void demoCatch1() {
        System.out.println("demoCatch1() start");
        try {
            methodA();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally. Object of exception was caught");
        }
        System.out.println("Execution is continued...");
        System.out.println("demoCatch1() end");
    }

    public static void methodA() throws IOException { // checked exception, you have to write throws IOException
        System.out.println("methodA() start");
        throw new IOException();
    }

    private static void demoCatch2() {
        //methodA();  // checked exception: you have to add exception to method signature or surround with try/catch
        //methodB();  // unchecked exception: you don't need surround it with try/catch
    }

    // unchecked exception, you don't need to write  throws RuntimeException
    public static void methodB() throws RuntimeException {
        System.out.println("methodB() start");
        throw new RuntimeException();
    }
}
