package com.epam.labs.exception.task3;

/**
 * 3) Modify the program from the previous task so
 * that throws generated exception inside the
 * same method.
 * Modify the program so exception will be thrown
 * when some condition was satisfied, for example
 * by delivering null link to the called method.
 * Will the "finally" block be executed ever?
 * Write a code for proving your answer.
 * <p>
 * Answer: "finally" is executed every time.
 *
 * @author Sergey Mikhluk.
 */
public class Main3 {

    public static void main(String[] args) {
        demoCatch1();
    }

    public static void demoCatch1() {
        System.err.println("1. demoCatch1() start");

        methodB("text");
        System.err.println();
        methodB(null);

        System.err.println("1. demoCatch1() end");
    }

    public static void methodB(String string) {
        System.err.println("1.1 methodB() start");
        System.err.println("string = " + string);
        try {
            // if string == null execution is interrupted and the message isn't printed
            System.err.println("string.length() = " + string.length());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.err.println("Exception was caught");  // executes every time, even if there isn't an exception
        }
        System.err.println("1.1 methodB() end");
    }
}
