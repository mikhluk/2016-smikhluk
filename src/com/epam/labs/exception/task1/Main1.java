package com.epam.labs.exception.task1;

/**
 * 1) Define link to an object and set it up with null value.
 * Try to call object method using this link.
 * Put give source code to demoCatch1 handler and catch
 * an exception. Which exception will be thrown
 * (checked/unchecked)?
 *
 * @author Sergey Mikhluk.
 */
public class Main1 {

    public static void main(String[] args) {
        System.out.println("main() start");
        String string = null;

        try {
            System.out.println(string.length()); // java.lang.NullPointerException , unchecked
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        System.out.println("Execution is continued...");
        System.out.println("main() end");
    }
}
