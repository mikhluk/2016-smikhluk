package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * a) stack overflow
 *
 * @author Sergey Mikhluk.
 */
public class ExampleStackRecursion {

    public static void main(String[] args) {
        System.out.println("ExampleStackRecursion. main(). start");
        demoStackOverflow();
        System.out.println("ExampleStackRecursion. main(). end");
    }

    /**
     * Result of execution
     * <p>
     * ExampleRecursion. main(). start
     * demoStackOverflow(). start
     * Exception in thread "main" java.lang.StackOverflowError
     * at com.epam.labs.exception.task4.ExampleStackRecursion.invokeRecursion(ExampleRecursion.java:34)
     * at com.epam.labs.exception.task4.ExampleStackRecursion.invokeRecursion(ExampleRecursion.java:34)
     */
    private static void demoStackOverflow() {
        System.out.println("demoStackOverflow(). start");
        invokeRecursion(0);
        System.out.println("demoStackOverflow(). end");
    }

    private static void invokeRecursion(int i) {
        if (i < 30_000) {
            invokeRecursion(i + 1);
        }
    }
}