package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * B) stack overflow
 * <p>
 * Example 1. Constructor of a class А calls a constructor of a class B,
 * and the constructor of class B calls the constructor of the class A .
 *
 * @author Sergey Mikhluk.
 */

class A {
    A() {
        System.out.println("constructor A");
        new B();
    }
}

class B {
    B() {
        System.out.println("constructor B");
        new A();
    }
}

public class ExampleStack1 {
    public static void main(String[] args) {
        System.out.println("ExampleStack1. main(). start");
        demoStackOverflow();
        System.out.println("ExampleStack1. main(). end");
    }

    /**
     * Result of execution
     * <p>
     * demoStackOverflow(). start
     * constructor A
     * constructor B
     * constructor A
     * Exception in thread "main" java.lang.StackOverflowError
     * at sun.nio.cs.UTF_8$Encoder.encodeLoop(UTF_8.java:691)
     * at com.epam.labs.exception.task4.D.methodD(ExampleStack2.java:25)
     * at com.epam.labs.exception.task4.C.toString(ExampleStack2.java:17)
     */
    private static void demoStackOverflow() {
        System.out.println("demoStackOverflow(). start");

        try {
            new A();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("demoStackOverflow(). end");
    }
}


