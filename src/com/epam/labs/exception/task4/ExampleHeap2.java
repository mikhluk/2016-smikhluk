package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * A) out of memory in the heap
 * <p>
 * Example 2. Adding the strings inside demoCatch1 loop.
 *
 * @author Sergey Mikhluk.
 */
public class ExampleHeap2 {

    public static void main(String[] args) {
        System.out.println("main(). start");
        demoHeapOutOfMemory();
        System.out.println("main(). end");
    }

    /**
     * Result of execution
     * Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
     * at com.epam.labs.exception.task4.Main.demoHeapOutOfMemory(Main.java:53)
     * at com.epam.labs.exception.task4.Main.main(Main.java:16)
     */
    private static void demoHeapOutOfMemory() {
        System.out.println("demoHeapOutOfMemory(). start");
        int size = 100;
        String string = "1";

        try {
            for (int i = 0; i < size; i++) {
                string += string;
                System.out.println("i = " + i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("demoHeapOutOfMemory(). end");
    }
}



