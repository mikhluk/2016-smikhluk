package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * A) out of memory in the heap
 * <p>
 * Example 3. Many-to-many connection between classes
 */

class Order {
    Client[] clients = new Client[1000_000_000];
}

class Client {
    Order[] orders = new Order[1000_000_000];
}

public class ExampleHeap3 {

    public static void main(String[] args) {
        System.out.println("main(). start");
        demoHeapOutOfMemory();
        System.out.println("main(). end");
    }

    /**
     * Result of execution
     * main(). start
     * Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
     * at com.epam.labs.exception.task4.Client.<init>(ExampleHeap3.java:15)
     * at com.epam.labs.exception.task4.ExampleHeap3.demoHeapOutOfMemory(ExampleHeap3.java:36)
     */
    private static void demoHeapOutOfMemory() {
        new Client();
    }
}

