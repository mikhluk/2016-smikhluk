package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * A) out of memory in the heap
 * <p>
 * Example 1. Creates an huge size array.
 *
 * @author Sergey Mikhluk.
 */
public class ExampleHeap1 {

    public static void main(String[] args) {
        System.out.println("main(). start");
        demoHeapOutOfMemory();
        System.out.println("main(). end");
    }

    /**
     * <p>
     * Result of execution
     * main(). start
     * Exception in thread "main" java.lang.OutOfMemoryError: Requested array size exceeds VM limit
     * at com.epam.labs.exception.task4.Main.demoHeapOutOfMemory(Main.java:42)
     * at com.epam.labs.exception.task4.Main.main(Main.java:15)
     */
    private static void demoHeapOutOfMemory() {
        System.out.println("demoHeapOutOfMemory(). start");
        int size = Integer.MAX_VALUE;

        try {
            long[] hugeArray = new long[size];

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("demoHeapOutOfMemory(). end");
    }
}