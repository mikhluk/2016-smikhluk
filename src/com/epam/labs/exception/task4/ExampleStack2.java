package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * B) stack overflow
 * <p>
 * The class C has overridden method toString(), that calls method
 * methodD() of the class D. The method methodD() outputs to the console
 * object of class C, that lead to call of method toString() of class С.
 *
 * @author Sergey Mikhluk.
 */

class C {
    @Override
    public String toString() {
        D.methodD();
        return "C{ }";
    }
}

class D {
    static void methodD() {
        System.out.println("D.methodD(). start");
        System.out.println(new C());
        System.out.println("D.methodD(). end");
    }
}

public class ExampleStack2 {
    public static void main(String[] args) {
        System.out.println("ExampleStack2. main(). start");
        demoStackOverflow();
        System.out.println("ExampleStack2. main(). end");
    }

    /**
     * Result of execution
     * D.methodD(). start
     * D.methodD(). start
     * ...
     * D.methodD(). start
     * <p>
     * Exception in thread "main" java.lang.StackOverflowError
     * at java.io.FileOutputStream.write(FileOutputStream.java:326)
     * at java.io.BufferedOutputStream.flushBuffer(BufferedOutputStream.java:82)
     * at java.io.PrintStream.write(PrintStream.java:482)
     * <p>
     * at com.epam.labs.exception.task4.D.methodD(ExampleStack2.java:24)
     * at com.epam.labs.exception.task4.C.toString(ExampleStack2.java:17)
     */
    private static void demoStackOverflow() {
        System.out.println("demoStackOverflow(). start");

        try {
            D.methodD();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("demoStackOverflow(). end");
    }
}