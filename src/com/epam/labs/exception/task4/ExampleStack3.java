package com.epam.labs.exception.task4;

/**
 * 4) Purposely simulate situations:
 * B) stack overflow
 * <p>
 * The class E has field of E class that initialized by constructor.
 *
 * @author Sergey Mikhluk.
 */

class E {
    E e = new E();
}

public class ExampleStack3 {

    public static void main(String[] args) {
        System.out.println("ExampleStack3. main(). start");
        demoStackOverflow();
        System.out.println("ExampleStack3. main(). end");
    }

    /**
     * Result of execution
     * Exception in thread "main" java.lang.StackOverflowError
     * at com.epam.labs.exception.task4.E.<init>(ExampleStack5.java:13)
     * at com.epam.labs.exception.task4.E.<init>(ExampleStack5.java:13)
     * ...
     */
    private static void demoStackOverflow() {
        E e = new E();
    }
}