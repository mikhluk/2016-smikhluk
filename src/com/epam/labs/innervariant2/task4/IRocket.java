package com.epam.labs.innervariant2.task4;

/**
 * @author Sergey Mikhluk.
 */
public interface IRocket {
    void landOff();

    void landOn();

    void setRocketName(String rocketName);
}
