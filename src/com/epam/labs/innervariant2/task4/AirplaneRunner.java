package com.epam.labs.innervariant2.task4;


/**
 * 4. Повторите предыдущее задание, определив вместо локального
 * внутреннего класса анонимный.*
 *
 * @author Sergey Mikhluk.
 */
public class AirplaneRunner {

    public static void main(String[] args) {
        IRocket rocket = new IRocket() {
            String rocketName;
            boolean inFlight;

            @Override
            public void landOff() {
                inFlight = true;
                new AirplaneRunner().getTemperature();
            }

            @Override
            public void landOn() {
                inFlight = false;
            }

            @Override
            public void setRocketName(String rocketName) {
                this.rocketName = rocketName;
            }

            @Override
            public String toString() {
                return "rocketName = " + rocketName + ", inFlight = " + inFlight;
            }
        };

        rocket.setRocketName("R-0002");

        rocket.landOff();
        System.out.println(rocket);
    }

    public void getTemperature(){
        System.out.println("Overboard temperature is: -30C");
    }


}
