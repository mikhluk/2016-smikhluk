package com.epam.labs.innervariant2.task1;

/**
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 *
 * @author Sergey Mikhluk.
 */
public interface IEngine {
    boolean isEngineEnabled();

    void setEngineEnabled(boolean enabled);
}
