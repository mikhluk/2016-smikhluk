package com.epam.labs.innervariant2.task1;

/**
 * Task 1. Inner class
 * 1. Write a class named Airplane that contains an inner class
 * named AirplaneEngine (engine).
 * 1.1 Define the Engine class inside the AirplaneEngine class.
 * <p>
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 * <p>
 * 1.3 Define an Airplane class, that has several engines.
 * <p>
 * 1.4 Call from an inner class an outer class method (or field)
 * 1.5 Call from an outer class an inner class method (or field)
 *
 * @author Sergey Mikhluk.
 */
public class AirplaneRunner {

    public static void main(String[] args) {
        Airplane airplane = new Airplane();
        airplane.setAirplaneBrand("Boeing");
        airplane.setAirplaneModel("737");

        Airplane.AirplaneEngine airplaneEngineLeft = airplane.new AirplaneEngine("diesel");
        Airplane.AirplaneEngine airplaneEngineRight = airplane.new AirplaneEngine("turbofan");

        airplane.setEngineLeft(airplaneEngineLeft);
        airplane.setEngineRight(airplaneEngineRight);

        // 1.5 Call from an outer class an inner class method (or field)
        // stop airplane.engineLeft
        airplane.stopEngine(airplaneEngineLeft);
        // start airplane.engineRight
        airplane.startEngine(airplaneEngineRight);

        System.out.println(airplane.toString());

        // Demonstrate
        // 1.4 Call from an inner class an outer class method (or field)
        airplaneEngineLeft.getEngine().printFullEngineInfo();


    }
}
