package com.epam.labs.innervariant2.task1;

/**
 * 1. Write a class named Airplane that contains an inner class
 * named AirplaneEngine (engine).
 * 1.1 Define the Engine class inside the AirplaneEngine class.
 * <p>
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 * <p>
 * 1.3 Define an Airplane class, that has several engines.
 * <p>
 * 1.4 Call from an inner class an outer class method (or field)
 * 1.5 Call from an outer class an inner class method (or field)
 *
 * @author Sergey Mikhluk.
 */
public class Airplane {
    private String airplaneBrand;
    private String airplaneModel;

    // 1.3 Define an Airplane class, that has several engines.
    private AirplaneEngine engineLeft;
    private AirplaneEngine engineRight;

    /**
     * 1. Write a class named Airplane that contains an inner class
     * named AirplaneEngine (engine).
     * Inner class AirplaneEngine is defined as non-static inner class.
     */
    public class AirplaneEngine implements IEngine {
        private Engine engine;
        private boolean engineEnabled;

        /**
         * 1.1 Define the Engine class inside the AirplaneEngine class.
         */
        public class Engine {
            // modifies public just for demonstrate the access from outer class
            public String type; // diesel, turbojet, turbofan, electric, V-type

            public void printFullEngineInfo(){
                //* 1.4 Call from an inner class an outer class method (or field)
                System.out.printf("printFullEngineInfo(): AirplaneBrand = %s, AirplaneName = %s, engineEnabled = %b, Engine.type = %s",
                        airplaneBrand, airplaneModel, engineEnabled, type);

            }
        }

        public AirplaneEngine(String type) {
            this.engine = new AirplaneEngine.Engine();
            // 1.5 Call from an outer class an inner class method (or field)
            this.engine.type = type;
        }

        @Override
        public boolean isEngineEnabled() {
            return engineEnabled;
        }

        @Override
        public void setEngineEnabled(boolean enabled) {
            engineEnabled = enabled;
        }

        public Engine getEngine() {
            return engine;
        }
    }

    // 1.5 Call from an outer class an inner class method (or field)
    public void startEngine(AirplaneEngine airplaneEngine) {
        airplaneEngine.setEngineEnabled(true);
    }

    // 1.5 Call from an outer class an inner class method (or field)
    public void stopEngine(AirplaneEngine airplaneEngine) {
        airplaneEngine.setEngineEnabled(false);
    }


    // getters and setters
    public void setEngineLeft(AirplaneEngine engineLeft) {
        this.engineLeft = engineLeft;
    }

    public void setEngineRight(AirplaneEngine engineRight) {
        this.engineRight = engineRight;
    }

    public void setAirplaneBrand(String airplaneBrand) {
        this.airplaneBrand = airplaneBrand;
    }

    public void setAirplaneModel(String airplaneModel) {
        this.airplaneModel = airplaneModel;
    }

    @Override
    public String toString() {
        return "Airplane {" +
                "\n\tairplaneBrand = '" + airplaneBrand + '\'' +
                ", \n\tairplaneModel = '" + airplaneModel + '\'' +

                //1.5 Call from an outer class an inner class method (or field)
                ", \n\t{engineLeft enabled = " + engineLeft.isEngineEnabled() +
                ", engineLeftType = " + engineLeft.engine.type +
                "}, \n\t{engineRight enabled = " + engineRight.isEngineEnabled() +
                ", engineRightType = " + engineRight.engine.type +
                "}\n}";
    }
}
