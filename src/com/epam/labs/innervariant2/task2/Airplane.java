package com.epam.labs.innervariant2.task2;

/**
 * @author Sergey Mikhluk.
 */
public class Airplane {

    private static class Rocket implements IRocket{
        String rocketName;
        boolean inFlight;

        @Override
        public void landOff() {
            inFlight = true;
        }

        @Override
        public void landOn() {
            inFlight = false;
        }

        public Rocket(String rocketName, boolean inFlight) {
            this.rocketName = rocketName;
            this.inFlight = inFlight;
        }

        @Override
        public String toString() {
            return "rocketName = " + rocketName + ", inFlight = " + inFlight;
        }
    }

    public static IRocket getRocket(String rocketName, boolean inFlight){
        return new Rocket(rocketName, inFlight);
        // todo разница будет, хотим ли мы из порождающего метода вызывать нестатик (статик) методы
        //в любом случае без  new не обойдемся
    }


}
