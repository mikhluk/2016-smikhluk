package com.epam.labs.innervariant2.task2;


/**
 * 2. Создайте открытый интерфейс, содержащий хотя бы один метод.
 * Реализуйте его в закрытом внутреннем классе. Дополните внешний
 * класс порождающим методом, который возвращает ссылку на
 * созданный объект внутреннего класса. Какой вид классов
 * предпочтительнее при этом использовать: внутренний или
 * вложенный?
 *
 * @author Sergey Mikhluk.
 */
public class AirplaneRunner {

    public static void main(String[] args) {
        IRocket rocket = Airplane.getRocket("R-0001", true);
        System.out.println(rocket);

        rocket.landOn();
        System.out.println(rocket);
    }
}
