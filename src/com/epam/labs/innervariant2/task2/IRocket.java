package com.epam.labs.innervariant2.task2;

/**
 * @author Sergey Mikhluk.
 */
public interface IRocket {
    void landOff();

    void landOn();
}
