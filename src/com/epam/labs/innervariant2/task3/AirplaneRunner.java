package com.epam.labs.innervariant2.task3;


/**
 * 3. Повторите предыдущее задание, определив внутренний класс
 * внутри порождающего метода.
 *
 * @author Sergey Mikhluk.
 */
public class AirplaneRunner {

    public static void main(String[] args) {
        IRocket rocket = new Airplane().getRocket("R-0001", true);
        System.out.println(rocket);

        rocket.landOn();
        System.out.println(rocket);
    }
}
