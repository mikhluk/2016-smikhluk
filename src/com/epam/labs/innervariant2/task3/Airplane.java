package com.epam.labs.innervariant2.task3;

/**
 * @author Sergey Mikhluk.
 */
public class Airplane {

    public IRocket getRocket(String rocketName, boolean inFlight){
        class Rocket implements IRocket {
            String rocketName;

            boolean inFlight;

            @Override
            public void landOff() {
                inFlight = true;
            }

            @Override
            public void landOn() {
                inFlight = false;
            }

            public Rocket(String rocketName, boolean inFlight) {
                this.rocketName = rocketName;
                this.inFlight = inFlight;
            }

            @Override
            public String toString() {
                return "rocketName = " + rocketName + ", inFlight = " + inFlight;
            }
        }

        return new Rocket(rocketName, inFlight);
    }

}
