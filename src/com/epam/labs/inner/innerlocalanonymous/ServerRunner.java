package com.epam.labs.inner.innerlocalanonymous;

/**
 * 2. Create an interface with at least one method.
 * 2.1 Add a private inner class that implements the interface.
 * 2.2 Add a method to outer class that returns a link to an object
 * of created inner class
 * <p>
 * <p>
 * 3. Modify Task 2, by defining an inner class inside
 * inside a generating method (local).
 * <p>
 * 4. Modify Task 3, by defining an anonymous class instead of inner
 * local class
 * <p>
 * 5. Call from an inner class a method (or field) of an outer class
 * 6. Call from an outer class a method (or field) of an inner class
 *
 * @author Sergey Mikhluk.
 */
public class ServerRunner {
    public static void main(String[] args) {
        Server server = new Server("Server-1");

        // 2. Creation an instance of an inner сlass by generating method getLogin()
        ICommand login = server.getLogin();
        server.send(login);

        // 3. Creation an instance of an inner class, as local class inside method getLogout()
        ICommand logout = server.getLogout();
        server.send(logout);
        // 6. Call from an outer class a method (or field) of an inner class
        System.out.println("access to inner variable commandName = " + logout.getCommandName());

        // 4. Creation an instance of anonymous class
        server.send(new ICommand() {
            private String commandName = "NewUser";

            @Override
            public void act() {
                // 5. Call from an inner class a method (or field) of an outer class
                System.out.println(server.getServerName() + ".act() = NewUser command");
            }

            @Override
            public String getCommandName() {
                return commandName;
            }
        });
    }
}
