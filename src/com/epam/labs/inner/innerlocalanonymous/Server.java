package com.epam.labs.inner.innerlocalanonymous;

/**
 * 2. Create an interface with at least one method.
 * 2.1 Add a private inner class that implements the interface.
 * 2.2 Add a generation method to outer class that returns a link
 * to an object of created inner class
 * <p>
 * <p>
 * 3. Modify Task 2, by defining an inner class inside
 * inside a generating method (local).
 * <p>
 * 4. Modify Task 3, by defining an anonymous class instead of inner
 * local class
 * <p>
 * 5. Call from an inner class a method (or field) of an outer class
 * 6. Call from an outer class a method (or field) of an inner class
 *
 * @author Sergey Mikhluk.
 */
public class Server {
    private String serverName;

    public Server(String serverName) {
        this.serverName = serverName;
    }

    // 2.1 Add a private inner class that implements the interface.
    private class Login implements ICommand{
        private String commandName = "Login";
        @Override
        public void act() {
            // 5. Call from an inner class a method (or field) of an outer class
            System.out.println(serverName +". act() = Login command");
        }
        @Override
        public String getCommandName() {
            return commandName;
        }
    }

    public void send(ICommand command){
        command.act();
        // 6. Call from an outer class a method (or field) of an inner class
        System.out.println("send command '" + command.getCommandName() + "' to Server");
    }

    // 2.2 Add a generation method to outer class that returns a link
    // to an object of created inner class
    public ICommand getLogin(){
        return new Login();
    }

    // 3. Modify Task 2, by defining an inner class inside
    // inside a generating method (local).
    public ICommand getLogout(){
        class Logout implements ICommand{
            private String commandName = "Logout";
            @Override
            public void act() {
                // 5. Call from an inner class a method (or field) of an outer class
                System.out.println(serverName + ". act() = Logout command");
            }
            @Override
            public String getCommandName() {
                return commandName;
            }
        }
        return new Logout();
    }

    public String getServerName() {
        return serverName;
    }
}
