package com.epam.labs.inner.innerlocalanonymous;

/**
 * 2. Create an interface with at least one method.
 * @author Sergey Mikhluk.
 */
public interface ICommand {
    void act();
    String getCommandName();
}
