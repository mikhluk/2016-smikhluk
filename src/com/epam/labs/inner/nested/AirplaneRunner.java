package com.epam.labs.inner.nested;

/**
 * 1. Write a class named Airplane that contains an inner class
 * named AirplaneEngine (engine).
 * 1.1 Define the Engine class inside the AirplaneEngine class.
 * <p>
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 * <p>
 * 1.3 Define an Airplane class, that has several engines.
 * <p>
 * 1.4 Call from an inner class a method (or field) of an outer class
 * 1.5 Call from an outer class a method (or field) of an inner class
 *
 * @author Sergey Mikhluk.
 */
public class AirplaneRunner {

    public static void main(String[] args) {
        Airplane.AirplaneEngine airplaneEngineLeft = new Airplane.AirplaneEngine("X0001-left", "diesel");
        Airplane.AirplaneEngine airplaneEngineRight = new Airplane.AirplaneEngine("X0002-right", "turbofan");
        airplaneEngineLeft.printEngineName();

        Airplane airplane = new Airplane();
        airplane.setAirplaneBrand("Boeing");
        airplane.setAirplaneModel("737");

        airplane.setEngineLeft(airplaneEngineLeft);
        airplane.setEngineRight(airplaneEngineRight);

        System.out.println(airplane.toString());
    }
}
