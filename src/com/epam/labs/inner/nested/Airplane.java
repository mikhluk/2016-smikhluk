package com.epam.labs.inner.nested;

/**
 * 1. Write a class named Airplane that contains an inner class
 * named AirplaneEngine (engine).
 * 1.1 Define the Engine class inside the AirplaneEngine class.
 * <p>
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 * <p>
 * 1.3 Define an Airplane class, that has several engines.
 * <p>
 * 1.4 Call from an inner class a method (or field) of an outer class
 * 1.5 Call from an outer class a method (or field) of an inner class
 *
 * @author Sergey Mikhluk.
 */
public class Airplane {
    private String airplaneBrand;
    private String airplaneModel;

    // 1.3 Define the Airplane class, that has several engines.
    private AirplaneEngine engineLeft;
    private AirplaneEngine engineRight;

    /**
     * 1. Write a class named Airplane that contains an inner class
     * named AirplaneEngine (engine).
     * Inner class AirplaneEngine is defined as static nested class.
     */
    public static class AirplaneEngine implements IEngine {
        private String engineName;
        private static final double GRAVITY_CONSTANT = 9.81;
        private Engine engine;

        /**
         * 1.1 Define the Engine class inside the AirplaneEngine class.
         */
        public class Engine {
            public String type; // diesel, turbojet, turbofan, electric, V-type
        }

        public AirplaneEngine(String engineName, String type) {
            this.engineName = engineName;
            this.engine = new AirplaneEngine.Engine();
            //* 1.5 Call from an outer class a method (or field) of an inner class
            this.engine.type = type;
        }

        /**
         * 1.2 Define in a separate interface IEngine the methods from the
         * AirplaneEngine class and implement them into an AirplaneEngine class.
         */
        @Override
        public void printEngineName() {
            // 1.4 Call from an inner class a method (or field) of an outer class
            // You can't access a non-static methods (and fields) from nested static class without creation of object

            // access a non-static method airplaneMethod()
            new Airplane().airplaneMethod();

            // access a static field GRAVITY_CONSTANT
            System.out.println("Airplane engine " + this.engineName + ", gravity constant = " + GRAVITY_CONSTANT);
        }
    }

    public void airplaneMethod() {
        System.out.println("Access from inner to outer: airPlaneMethod()");
    }

    public void setEngineLeft(AirplaneEngine engineLeft) {
        this.engineLeft = engineLeft;
    }

    public void setEngineRight(AirplaneEngine engineRight) {
        this.engineRight = engineRight;
    }


    public void setAirplaneBrand(String airplaneBrand) {
        this.airplaneBrand = airplaneBrand;
    }

    public void setAirplaneModel(String airplaneModel) {
        this.airplaneModel = airplaneModel;
    }

    @Override
    public String toString() {
        return "Airplane{" +
                "airplaneBrand = '" + airplaneBrand + '\'' +
                ", airplaneModel = '" + airplaneModel + '\'' +
                // 1.5 Call from an outer class a method (or field) of an inner class
                ", {engineLeftName = " + engineLeft.engineName +
                ", engineLeftType = " + engineLeft.engine.type +
                "}, {engineRightName = " + engineRight.engineName +
                ", engineRightType = " + engineRight.engine.type +
                "}}";
    }
}
