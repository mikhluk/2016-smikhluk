package com.epam.labs.inner.nested;

/**
 * 1.2 Define in a separate interface IEngine the methods from the
 * AirplaneEngine class and implement them into an AirplaneEngine class.
 *
 * @author Sergey Mikhluk.
 */
public interface IEngine {
    void printEngineName();
}
