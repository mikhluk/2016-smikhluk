package com.epam.labs.annotation;

import java.lang.reflect.Method;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {
    @RequestForSomeMethod(version = 7,
    title = "Java",
    date = "06.07.2014")
    public void someMethod(){
        try{
            Class c = this.getClass();
            Method m = c.getMethod("someMethod");
            RequestForSomeMethod ann = m.getAnnotation(RequestForSomeMethod.class);

            System.out.println(ann.version() + " " + ann.title() + " " + ann.date() );
        } catch (NoSuchMethodException e){
            System.out.println("метод не найден");
        }
    }

    public static void main(String[] args) {
        Runner object = new Runner();
        object.someMethod();
    }

}
