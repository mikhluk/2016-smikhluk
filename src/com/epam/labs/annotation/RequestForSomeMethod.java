package com.epam.labs.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Sergey Mikhluk.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestForSomeMethod {
    int version();
    String title();
    String date();

}
