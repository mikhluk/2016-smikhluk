package com.epam.labs.flowcontrol_home.task4;

/**
 * Work traffic for drivers programmed as follows: starting from the beginning of every hour
 * for three minutes the green light, then in one minute - yellow, for two minutes - red,
 * for three minutes - again, green and so on.
 * For real number t, is the time in minutes since the beginning of the next hour.
 * Determine what color the signal lights for drivers at this point.
 *
 * @author Sergey Mikhluk.
 */
public class Main {
    public static void main(String[] args) {
        TrafficLight trafficLight = new TrafficLight();
        float minute = 58.00f;
        try {
            System.out.printf("For %.3f minute light is - ", minute);
            System.out.println(trafficLight.determineLight(minute));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
