package com.epam.labs.flowcontrol_home.task4;

/**
 * @author Sergey Mikhluk.
 */
public class TrafficLight {
    public static final int MAX_MINUTE = 59;
    public static final int TIME_CYCLE = 6;
    public static final int MAX_MINUTE_GREEN = 3;
    public static final int MAX_MINUTE_YELLOW = 4;

    private enum Light {GREEN, RED, YELLOW}

    public Light determineLight(float minute) {
        if (minute < 0 || (int) minute > MAX_MINUTE) {
            throw new IllegalArgumentException();
        }

        minute %= TIME_CYCLE;

        if (minute < MAX_MINUTE_GREEN) {
            return Light.GREEN;
        } else if (minute < MAX_MINUTE_YELLOW) {
            return Light.YELLOW;
        } else {
            return Light.RED;
        }
    }
}
