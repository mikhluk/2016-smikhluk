Assignments on "Control Flow"

Practice
1. We have integer number a. Determine the number of entries it in value b.

2. Write an application that should display the total number of input numbers, the largest number,
smallest number and the average of all the numbers.

3. Type the numbers as like in these tables:
a).
0
1 0
2 1 0
3 2 1 0
4 3 2 1 0

b).
        1
      2 1
    3 2 1
  4 3 2 1
5 4 3 2 1

4. Work traffic for drivers programmed as follows: starting from the beginning of every hour for three minutes
the green light, then in one minute - yellow, for two minutes - red, for three minutes - again, green and so on.
For real number t, is the time in minutes since the beginning of the next hour. Determine what color the signal
lights for drivers at this point.


Вопросы
1. Нужно ли ловить превышение размера при сложении float ?
float f1 = 300000000000000000000000000000000000000f;
float f2 = 300000000000000000000000000000000000000f;
float f3 = f1 + f2;   = Infinity;

2. Почему в данном примере результат true, если сравниваются два объекта по ссылкам?
String s1 = "hello";
String s2 = "hello";
System.out.println("s1 == s2 - " + (s1 == s2));  // true - because ???