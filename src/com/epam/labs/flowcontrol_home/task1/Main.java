package com.epam.labs.flowcontrol_home.task1;

/**
 * We have integer number a. Determine the number of receiveIndexes it in value b.
 *
 * @author Sergey Mikhluk.
 */
public class Main {
    public static void main(String[] args) {
        runVariant1();
        runVariant2();
    }

    // MS: First variant of realization, Works if
    // A is int number from 0 to MAX_INT 2147483647, B is int number from 0 to MAX_INT 2147483647
    private static void runVariant1() {
        int numberA = 12;
        int numberB = 12121212;

        int count = NumberUtils.countEntries(numberB, numberA);
        outputResult(numberA, numberB, count);
    }

    // MS: Another variant of realization, Works if
    // A is digit from 0 to 9, B is int number from 0 to MAX_INT 2147483647
    private static void runVariant2() {
        int numberA = 4;
        int numberB = 304457144;
        int count = 0;

        try {
            count = NumberUtils.countEntriesOfDigit(numberB, numberA);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        outputResult(numberA, numberB, count);
    }

    private static void outputResult(int numberA, int numberB, int count) {
        System.out.println("A = " + numberA + ", B = " + numberB);
        System.out.println("B includes A for " + count + " times.");
    }
}
