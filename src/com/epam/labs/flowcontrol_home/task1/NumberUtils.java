package com.epam.labs.flowcontrol_home.task1;

/**
 * @author Sergey Mikhluk.
 */
public class NumberUtils {
    public static final int TEN = 10;
    public static final int MAX_DIGIT = 9;

    public static int countEntries(int number, int subNumber) {
        if (subNumber > number) {
            return 0;
        }
        if (subNumber == number) {
            return 1;
        }

        int[] array = numberToArray(number);
        int[] subArray = numberToArray(subNumber);

        return calculate(array, subArray);
    }

    private static int calculate(int[] array, int[] subArray) {
        int count = 0;
        boolean digitMatch = false;

        for (int i=0; i < (array.length - subArray.length + 1); i++){
            for(int j=0; j < subArray.length; j++){
                digitMatch = false;
                if (array[i+j] != subArray[j]) {
                    break;
                }
                digitMatch = true;
            }
            if (digitMatch) {
                count++;
            }
        }
        return count;
    }

    private static int getSize(int number) {
        int count = 1;
        while ((number /= TEN) > 0){
            count++;
        }
        return count;
    }

    private static int[] numberToArray(int number) {
        int size = getSize(number);
        int[] array = new int[size];

        for (int i = 0; i < size; i++){
            array[i] = number % TEN;
            number /= TEN;
        }
        return array;
    }

    public static int countEntriesOfDigit(long number, int digit) {
        if (digit > MAX_DIGIT || digit < 0 || number < 0) {
            throw new IllegalArgumentException();
        }

        int count = 0;
        long value = number;

        while (value > 0) {
            if (value % TEN == digit) {
                count++;
            }
            value /= TEN;
        }
        return count;
    }
}
