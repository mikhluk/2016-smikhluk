package com.epam.labs.flowcontrol_home.task2;

/**
 * Write an application that should display the
 * - total number of input numbers,
 * - the largest number,
 * - smallest number and
 * - the average of all the numbers.
 *
 * @author Sergey Mikhluk.
 */
public class Main {
    public static void main(String[] args) {
        NumberList numberList = NumberListFactory.getNumberListFromConsole();

        System.out.printf("Quantity of numbers: %d %n", numberList.getSize());
        System.out.printf("Max value: %.2f %n", NumberUtils.getMaxValue(numberList));
        System.out.printf("Min value: %.2f %n", NumberUtils.getMinValue(numberList));
        System.out.printf("Average value: %.2f %n", NumberUtils.getAverageValue(numberList));
    }
}
