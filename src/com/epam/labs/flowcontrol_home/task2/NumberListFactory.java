package com.epam.labs.flowcontrol_home.task2;

import java.util.Scanner;

/**
 * @author Sergey Mikhluk.
 */
public class NumberListFactory {
    public static NumberList getNumberListFromConsole(){
        float value;
        Scanner scanner = new Scanner(System.in);
        NumberList numberList = new NumberList();

        System.out.println("Enter float number (or type 'e' without quotes for exit):");
        String s1 = scanner.nextLine();

        while (!s1.equals("e")) {
            try {
                value = Float.valueOf(s1);
                System.out.println(value);

                numberList.add(value);
            } catch (NumberFormatException e) {
                System.out.println("Number is not float! Try again!");
                //e.printStackTrace();
            }
            s1 = scanner.nextLine();
        }
        return numberList;
    }
}
