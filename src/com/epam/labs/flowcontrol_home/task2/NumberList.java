package com.epam.labs.flowcontrol_home.task2;

/**
 * @author Sergey Mikhluk.
 */
public class NumberList {
    private static final int DEFAULT_CAPACITY = 10;
    private static final int MAX_CAPACITY = 17;
    private float[] data;
    private int size = 0;

    public NumberList() {
        data = new float[DEFAULT_CAPACITY];
    }

    public NumberList(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException();
        }
        data = new float[capacity];
    }

    public float[] getData() {
        return data;
    }

    public float getElement(int position) {
        return data[position];
    }

    public void add(float value) {
        if (size >= getCapacity()) {
            createBiggerArray();
        }
        data[size] = value;
        size++;
    }

    private void createBiggerArray() {
        if (getCapacity() >= MAX_CAPACITY) {
            throw new IndexOutOfBoundsException();
        }

        int biggerCapacity = getCapacity() * 3 / 2;
        biggerCapacity = (biggerCapacity >= MAX_CAPACITY) ? MAX_CAPACITY : biggerCapacity;

        float[] biggerArray = new float[biggerCapacity];

        System.arraycopy(data, 0, biggerArray, 0, getCapacity());
        data = biggerArray;
    }

    public int getCapacity() {
        return data.length;
    }

    public int getSize() {
        return size;
    }

}



