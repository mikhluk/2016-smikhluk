package com.epam.labs.flowcontrol_home.task2;

/**
 * @author Sergey Mikhluk.
 */
public class NumberUtils {

    public static float getMinValue(NumberList numberList) {
        if (numberList == null || numberList.getSize() == 0) {
            return 0;
        }

        float minValue = numberList.getElement(0);
        for (int i = 0; i < numberList.getSize(); i++) {
            minValue = (numberList.getElement(i) < minValue) ? numberList.getElement(i) : minValue;
        }

        return minValue;
    }


    public static float getMaxValue(NumberList numberList) {
        float maxValue = 0;
        for (float value : numberList.getData()) {
            maxValue = (value > maxValue) ? value : maxValue;
        }
        return maxValue;
    }


    public static float getAverageValue(NumberList numberList) {
        if (numberList == null || numberList.getSize() == 0) {
            return 0;
        }

        float average = 0;
        float size = numberList.getSize();

        for (int i = 0; i < size; i++) {
            average += numberList.getElement(i) / size;
        }
        return average;
    }
}
