package com.epam.labs.flowcontrol_home.task3;

/**
 * @author Sergey Mikhluk.
 */
public class TableFactory {
    public static Table createTable(int size, boolean functionA) {
        Table table = new Table(size);
        int value;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                value = function(i, j, size, functionA);
                table.setElement(i, j, value);
            }
        }
        return table;
    }

    private static int function(int i, int j, int size, boolean functionA) {
        if (functionA) {
            return (i >= j) ? (i - j) : -1;
        } else {
            return (i + j >= size - 1) ? size - j : -1;
        }
    }
}
