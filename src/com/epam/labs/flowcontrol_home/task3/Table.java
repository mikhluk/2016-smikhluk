package com.epam.labs.flowcontrol_home.task3;

/**
 * @author Sergey Mikhluk.
 */
public class Table {
    int[][] table;

    public Table(int size) {
        table = new int[size][size];
    }

    public void setElement(int i, int j, int value) throws ArrayIndexOutOfBoundsException {
        table[i][j] = value;
    }

    @Override
    public String toString() {
        int size = table.length;
        StringBuffer buffer = new StringBuffer(size * size * 2);

        for (int[] tempTable : table) {
            for (int value : tempTable) {
                buffer.append(value >= 0 ? value : " ");
                buffer.append(" ");
            }
            buffer.append("\n");
        }
        return buffer.toString();
    }
}
