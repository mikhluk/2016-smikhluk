package com.epam.labs.flowcontrol_home.task3;

/**
 * Type the numbers as like in these tables:
 * a)
 * 0
 * 1 0
 * 2 1 0
 * 3 2 1 0
 * 4 3 2 1 0
 *
 * b)
 * 1
 * 2 1
 * 3 2 1
 * 4 3 2 1
 * 5 4 3 2 1
 *
 * @author Sergey Mikhluk.
 */
public class Main {
    public static final int SIZE = 5;

    public static void main(String[] args) {
        System.out.println(TableFactory.createTable(SIZE, true));
        System.out.println(TableFactory.createTable(SIZE, false));
    }
}
