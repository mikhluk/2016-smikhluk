package com.epam.labs.string;

import org.junit.Test;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * @author Sergey Mikhluk.
 */
public class StringUtilsTest {
    public static final Level LEVEL = Level.INFO;
    private static Logger logger = Logger.getLogger(StringUtilsTest.class.getName());

    @Test
    public void stringSize() {
        String oldString = "dong-ding-dong";
        StringUtils stringUtils = new StringUtils(oldString);

        assertEquals(14, stringUtils.stringSize());
    }

    @Test
    public void toLowerCase() {
        String oldString = "doNg-dIng-donG";
        String resultString = oldString.toLowerCase();
        StringUtils stringUtils = new StringUtils(oldString);

        assertEquals(resultString, stringUtils.toLowerCase());
    }


    @Test
    public void toUpperCase() {
        String oldString = "doNg-dIng-donG";
        String resultString = oldString.toUpperCase();
        StringUtils stringUtils = new StringUtils(oldString);

        assertEquals(resultString, stringUtils.toUpperCase());
    }

    @Test
    public void compareTo() {
        String oldString = "doNg-dIng-donG";
        String otherString = "dong-dIng-dong";
        StringUtils stringUtils = new StringUtils(oldString);
        assertTrue(stringUtils.compareTo(otherString));

        otherString = "dong-dIng-dont";
        assertFalse(stringUtils.compareTo(otherString));
    }

    @Test
    public void receiveIndexes() {
        logger.log(LEVEL, "receiveIndexes(). invoked");
        String oldString = "dong-ding-dong";
        String subString = "dong";
        StringUtils stringUtils = new StringUtils(oldString);

        int[] expected = {0, 10};
        int[] actual = stringUtils.receiveIndexes(subString);
        logger.log(LEVEL, "actual = " + Arrays.toString(stringUtils.receiveIndexes(subString)));

        assertArrayEquals(expected, actual);
        logger.log(LEVEL, "receiveIndexes(). exit");
    }

    @Test
    public void changeTo() {
        String oldString = "dong-ding-dong";
        String subString = "dong";
        String replacement = "bong";

        StringUtils stringUtils = new StringUtils(oldString);
        assertEquals("bong-ding-bong", stringUtils.changeTo(subString, replacement));
    }

    @Test
    public void countIdentical() {
        logger.log(LEVEL, "countIdentical(). invoked");
        String oldString = "dong-ding-dong";
        StringUtils stringUtils = new StringUtils(oldString);

        Node[] expected = {new Node("ding", 1), new Node("dong", 2)};
        Node[] actual = stringUtils.countIdentical();
        logger.log(LEVEL, "actual = " + Arrays.toString(actual));

        for (int i = 0; i < expected.length; i++) {
            assertEquals("mismatch at Node[" + i + "].value", expected[i].getValue(), actual[i].getValue());
            assertEquals("mismatch at Node[" + i + "].receiveIndexes", expected[i].getQuantity(), actual[i].getQuantity());
        }
        logger.log(LEVEL, "countIdentical(). exit");
    }
}
