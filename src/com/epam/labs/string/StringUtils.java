package com.epam.labs.string;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sergey Mikhluk.
 */
public class StringUtils {
    private String data;

    public StringUtils() {
        data = "";
    }

    public StringUtils(String data) {
        this.data = (data == null) ? "" : data;
    }

    /**
     * @return Returns the number of characters in this data
     */
    public int stringSize() {
        return data.length();
    }

    /**
     * @return this data, converted to lowercase.
     */
    public String toLowerCase() {
        return data.toLowerCase();
    }

    /**
     * @return this data, converted to uppercase.
     */
    public String toUpperCase() {
        return data.toUpperCase();
    }

    /**
     * Compares given string with this data ignoring case differences
     *
     * @param string string to be compared
     * @return true/false if the specified String is equal/not equal to this data, ignoring case considerations.
     */
    public boolean compareTo(String string) {
        return data.compareToIgnoreCase(string) == 0;
    }

    /**
     * Receives indexes in this data where given subString starts
     *
     * @param subString the string for matching
     * @return the array with indexes in this data where given subString starts
     */
    public int[] receiveIndexes(String subString) {
        if (subString == null || subString.equals("")) {
            throw new IllegalArgumentException("Null or empty parameter subString!");
        }

        String string = data.toLowerCase();
        subString = subString.toLowerCase();

        int[] indexes = new int[stringSize()];
        indexes[0] = -1;

        int entriesCount = 0;
        int index = string.indexOf(subString);
        while (index >= 0) {
            indexes[entriesCount] = index;
            index = string.indexOf(subString, index + 1);
            entriesCount++;
        }

        int[] indexesResult = new int[entriesCount];
        System.arraycopy(indexes, 0, indexesResult, 0, entriesCount);

        return indexesResult;
    }

    /**
     * Replaces each substring of this data that matches the substring with replacement string.
     *
     * @param subString   string to be replaced
     * @param replacement the replacement string
     * @return the resulting string
     */
    public String changeTo(String subString, String replacement) {
        if (subString == null || subString.equals("")) {
            return data;
        }
        if (replacement == null) {
            throw new NullPointerException("Null parameter 'replacement'!");
        }
        return data.replace(subString, replacement);
    }

    /**
     * Finds identical words in this data ignoring case differences
     *
     * @return array of Node objects with quantity of matches any words
     */
    public Node[] countIdentical() {
        if (data.equals("")) {
            return new Node[]{new Node("", 0)};
        }

        String word;
        Map<String, Integer> map = new HashMap<>();
        Pattern pattern = Pattern.compile("[a-zA-Z]+");
        Matcher matcher = pattern.matcher(data);

        while (matcher.find()) {
            word = matcher.group().toLowerCase();

            if (map.containsKey(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }

        ArrayList<Node> list = new ArrayList<>(map.size());
        map.forEach((key, value) -> list.add(new Node(key, value)));
        return list.toArray(new Node[0]);
    }

    /* getters and setters */
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}