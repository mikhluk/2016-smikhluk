package com.epam.labs.string;

import java.util.Arrays;

/**
 * Create an application that modify the string "Dong-ding-dong" in next way:
 * 1) outputs quantity of letters in the string
 * 2) compares given string to any other, ignoring case
 * 3) changes all letters to uppercase/lowercase and outputs a result to the console
 * 4) outputs to the console all indexes of the word "dong"
 * 5) changes every occurrence of the word "dong" by "bong"
 * 6) finds all identical words, and counts quantity of them
 *
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {
        demo1();
        demo2();
        demo3();
    }

    private static void demo(String oldString, String otherString, String subString, String replacement) {
        StringUtils stringUtils = new StringUtils(oldString);
        System.out.println("original string: " + stringUtils.getData());
        System.out.println("1) size: " + stringUtils.stringSize());

        System.out.println("otherString: " + otherString);
        System.out.println("2) original.equals(otherString): " + stringUtils.compareTo(otherString));

        System.out.println("3) toLowerCase(): " + stringUtils.toLowerCase());
        System.out.println("3) toUpperCase(): " + stringUtils.toUpperCase());

        System.out.println("subString: " + subString);
        System.out.println("4) indexes of subString: " + Arrays.toString(stringUtils.receiveIndexes(subString)));

        System.out.println("replacement: " + replacement);
        System.out.println("5) changed string: " + stringUtils.changeTo(subString, replacement));

        System.out.println("6) found words : " + Arrays.toString(stringUtils.countIdentical()));
    }

    /**
     * Expected result:
     * original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * 1) size: 45
     * otherString: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * 2) original.equals(otherString): true
     * 3) toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
     * 3) toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
     * subString: ding
     * 4) indexes of subString: [0, 5, 21, 27]
     * replacement: bong
     * 5) changed string: bong-diNg-DOng-di ng-bong--bong!ng?di(di)(di)
     * 6) found words : [{value = ding, quantity = 4}, {value = di, quantity = 4}, {value = ng, quantity = 2}, {value = dong, quantity = 1}]
     */
    private static void demo1() {
        String oldString = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String otherString = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subString = "ding";
        String replacement = "bong";

        System.out.println("DEMO 1.");
        try {
            demo(oldString, otherString, subString, replacement);
        } catch (NullPointerException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Expected result:
     * original string:
     * 1) size: 0
     * otherString: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * 2) original.equals(otherString): false
     * 3) toLowerCase():
     * 3) toUpperCase():
     * subString:
     * Exception in thread "main" java.lang.IllegalArgumentException: Null or empty parameter subString!
     */
    private static void demo2() {
        String oldString = null;
        String otherString = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subString = "";
        String replacement = "bong";

        System.out.println("\nDEMO 2.");

        try {
            demo(oldString, otherString, subString, replacement);
        } catch (NullPointerException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Expected result:
     * original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * 1) size: 45
     * otherString: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * 2) original.equals(otherString): true
     * 3) toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
     * 3) toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
     * subString: ding
     * 4) indexes of subString: [0, 5, 21, 27]
     * replacement: null
     * Exception in thread "main" java.lang.NullPointerException: Null parameter 'replacement'!
     */
    private static void demo3() {
        String oldString = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String otherString = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subString = "ding";
        String replacement = null;

        System.out.println("\nDEMO 3.");
        try {
            demo(oldString, otherString, subString, replacement);
        } catch (NullPointerException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
