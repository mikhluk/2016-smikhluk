package com.epam.labs.string;

/**
 * @author Sergey Mikhluk.
 */
public class Node {
    private String value;
    private int quantity;

    public Node(String value, int quantity) {
        this.value = value;
        this.quantity = quantity;
    }

    public String getValue() {
        return value;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "{value = " + value + ", quantity = " + quantity + '}';
    }
}
