package com.epam.labs.inheritance.arraytask;

/**
 * Create an object of class One-dimension array, by using Array class.
 * Methods:
 * - create,
 * - output to console,
 * - operations (add, subtract, multiply).
 *
 * Override the methods: equals(), hashCode(), toString().
 *
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {
        equalsDemo();
        operationsDemo();
    }

    private static void equalsDemo() {
        System.out.println("1. Equals and operator '==' demo.");
        OneDimensionalArray oneDimensionalArrayA = new OneDimensionalArray(new int[]{1, 4, 6});
        System.out.println("A = new oneDimensionalArray() = " + oneDimensionalArrayA);

        OneDimensionalArray oneDimensionalArrayA1 = oneDimensionalArrayA;  // link of object A1 = link of A
        System.out.println("A1 = A = " + oneDimensionalArrayA1);

        OneDimensionalArray oneDimensionalArrayB = new OneDimensionalArray(new int[]{1, 2, 1});
        System.out.println("B = new oneDimensionalArray() = " + oneDimensionalArrayB);

        OneDimensionalArray oneDimensionalArrayC = new OneDimensionalArray(new int[]{1, 2, 1});  // values of object C == values of object B
        System.out.println("C = new oneDimensionalArray() = " + oneDimensionalArrayC);

        System.out.println();
        System.out.println("A equals A = " + oneDimensionalArrayA.equals(oneDimensionalArrayA));
        System.out.println("A equals A1 = " + oneDimensionalArrayA.equals(oneDimensionalArrayA1));
        System.out.println("A equals B = " + oneDimensionalArrayA.equals(oneDimensionalArrayB));
        System.out.println("A equals C = " + oneDimensionalArrayA.equals(oneDimensionalArrayC));
        System.out.println("B equals C = " + oneDimensionalArrayB.equals(oneDimensionalArrayC));

        System.out.println("B equals Matrix = " + oneDimensionalArrayB.equals(new Matrix()));

        System.out.println();
        System.out.println("A == A = " + (oneDimensionalArrayA == oneDimensionalArrayA));
        System.out.println("A == A1 = " + (oneDimensionalArrayA == oneDimensionalArrayA1));
        System.out.println("A == B = " + (oneDimensionalArrayA == oneDimensionalArrayB));
        System.out.println("A == C = " + (oneDimensionalArrayA == oneDimensionalArrayC));
        System.out.println("B == C = " + (oneDimensionalArrayB == oneDimensionalArrayC));
    }

    private static void operationsDemo() {
        System.out.println("\nOperations demo.");
        OneDimensionalArray oneDimensionalArrayA = new OneDimensionalArray(new int[]{1, 4, 6});
        System.out.println("A = " + oneDimensionalArrayA);

        OneDimensionalArray oneDimensionalArrayB = new OneDimensionalArray(new int[]{1, 2, 1});
        System.out.println("B = " + oneDimensionalArrayB);

        System.out.println("A + B = " + oneDimensionalArrayA.add(oneDimensionalArrayB));
        System.out.println("A - B = " + oneDimensionalArrayA.subtract(oneDimensionalArrayB));
        System.out.println("A * B = " + oneDimensionalArrayA.multiply(oneDimensionalArrayB));
    }
}
