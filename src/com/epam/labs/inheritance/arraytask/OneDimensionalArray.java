package com.epam.labs.inheritance.arraytask;

import java.util.Arrays;

/**
 * Create an object of class One-dimension array, by using Array class.
 * Methods:
 * - create,
 * - output to console,
 * - operations (add, subtract, multiply).
 *
 * Override the methods: equals(), hashCode(), toString().
 *
 * @author Sergey Mikhluk.
 */
public class OneDimensionalArray extends Array<OneDimensionalArray> {
    private int[] body;

    public OneDimensionalArray(int[] body) {
        this.body = body;
    }

    private boolean checkArguments(OneDimensionalArray array) {
        return getSize() == array.getSize();
    }

    @Override
    public OneDimensionalArray add(OneDimensionalArray array) {
        if (!checkArguments(array)){
            throw new IllegalArgumentException();
        }

        int[] arrayResult = new int[getSize()];
        for (int i = 0; i < getSize(); i++) {
            arrayResult[i] = body[i] + array.getBody()[i];
        }

        return new OneDimensionalArray(arrayResult);
    }

    @Override
    public OneDimensionalArray subtract(OneDimensionalArray array) {
        if (!checkArguments(array)){
            throw new IllegalArgumentException();
        }

        int[] arrayResult = new int[getSize()];
        for (int i = 0; i < getSize(); i++) {
            arrayResult[i] = body[i] - array.getBody()[i];
        }

        return new OneDimensionalArray(arrayResult);
    }

    @Override
    public OneDimensionalArray multiply(OneDimensionalArray array) {
        if (!checkArguments(array)){
            throw new IllegalArgumentException();
        }

        int[] arrayResult = new int[getSize()];
        for (int i = 0; i < getSize(); i++) {
            arrayResult[i] = body[i] * array.getBody()[i];
        }

        return new OneDimensionalArray(arrayResult);
    }

    public int[] getBody() {
        return body;
    }

    public int getSize() {
        return body.length;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        OneDimensionalArray oneDimensionalArray = (OneDimensionalArray) obj;

        if (getSize() != oneDimensionalArray.getSize()) {
            return false;
        }

        for (int i = 0; i < getSize(); i++) {
            if (body[i] != oneDimensionalArray.getBody()[i]) {
                return false;
            }
        }
        return true;
    }

    @Override
      public int hashCode() {
        int result = 17;
        for (int value : body) {
            result = 37 * result + value;
        }
        return result;
    }

    @Override
    public String toString() {
        return "OneDimensionalArray{" +
                "body=" + Arrays.toString(body) +
                "}, hashcode " + hashCode();
    }
}
