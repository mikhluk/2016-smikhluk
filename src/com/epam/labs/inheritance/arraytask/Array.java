package com.epam.labs.inheritance.arraytask;

/**
 * Methods:
 * - create,
 * - output to console,
 * - operations (add, subtract, multiply).
 *
 * @author Sergey Mikhluk.
 */
public abstract class Array<T> {
    abstract  Array add(T element);
    abstract  Array subtract(T element);
    abstract  Array multiply(T element);
}
