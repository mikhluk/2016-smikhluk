package com.epam.labs.inheritance.arraytask;

/**
 * Very simple example of another class that inherited from Array class
 *
 * @author Sergey Mikhluk.
 */
public class Matrix extends Array<Matrix> {
    int[][] body;

    @Override
    Array add(Matrix array) {
        return null;
    }

    @Override
    Array subtract(Matrix array) {
        return null;
    }

    @Override
    Array multiply(Matrix array) {
        return null;
    }
}
