package com.epam.labs.stringrurenko;

import java.util.Arrays;

/**
 * Создайте программу, которая преобразовывает в строке "Dong-ding-dong"
 * - выводит количество букв в строке +
 * - сравнивает данную строку с любой другой, игнорируя регистр
 * - все буквы к верхнему/нижнему регисту и выводит результат на консоль
 * - выводит на консоль все индексы слова "dong"
 * - заменяет каждое вхождение слова dong на bong
 * - находит все одинаковые слова, подсчитывая их количество
 *
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {
        //demo1();
        demo2();
        //demo3();
    }

    /* Expected result:
        original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
        size: 45
        otherStr: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
        original.equals(otherStr): true
        toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
        toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
        subStr: ding
        receiveIndexes of subStr: {0, 5, 21, 27}
        replacement: bong
        changed string: bong-diNg-DOng-di ng-bong--bong!ng?di(di)(di)
        found tokens : [{value = ding, receiveIndexes = 3}, {value = diNg, receiveIndexes = 1}, {value = DOng, receiveIndexes = 1},
        {value = di, receiveIndexes = 4}, {value = ng, receiveIndexes = 2}, null, null, null, null, null, null]
    */
    private static void demo1() {
        String oldStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String otherStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        StringTask str = new StringTask();
        str.setString(oldStr);

        String subStr = "ding";
        String replacement = "bong";

        System.out.println("original string: " + str.getString());
        System.out.println("size: " + str.stringSize());

        System.out.println("otherStr: " + otherStr);
        System.out.println("original.equals(otherStr): " + str.compareTo(otherStr));

        System.out.println("toLowerCase(): " + str.toLowerCase());
        System.out.println("toUpperCase(): " + str.toUpperCase());

        System.out.println("subStr: " + subStr);
        System.out.println("receiveIndexes of subStr: " + Arrays.toString(str.entries(subStr)));

        System.out.println("replacement: " + replacement);
        System.out.println("changed string: " + str.changeTo(subStr, replacement));

        System.out.println("found tokens : " + Arrays.toString(str.frequencyOfRepetitions()));
    }

    /* Expected result:
        original string:
        size: 0
        otherStr: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
        original.equals(otherStr): false
        toLowerCase():
        toUpperCase():
        subStr:
        receiveIndexes of subStr: {-1}
        replacement: bong
        changed string:
        found tokens : []
    */
    private static void demo2() {
        String otherStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String oldStr = null;

        StringTask str = new StringTask();
        str.setString(oldStr);

        String subStr = "";
        String replacement = "bong";

        System.out.println("original string: " + str.getString());
        System.out.println("size: " + str.stringSize());

        System.out.println("otherStr: " + otherStr);
        System.out.println("original.equals(otherStr): " + str.compareTo(otherStr));

        System.out.println("toLowerCase(): " + str.toLowerCase());
        System.out.println("toUpperCase(): " + str.toUpperCase());

        System.out.println("subStr: " + subStr);
        System.out.println("receiveIndexes of subStr: " + Arrays.toString(str.entries(subStr)));

        System.out.println("replacement: " + replacement);
        System.out.println("changed string: " + str.changeTo(subStr, replacement));

        System.out.println("found tokens : " + Arrays.toString(str.frequencyOfRepetitions()));
    }

    /* Expected result:
        original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
        size: 45
        otherStr: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
        original.equals(otherStr): true
        toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
        toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
        subStr: ding
        receiveIndexes of subStr: {0, 5, 21, 27}
        replacement: null
        java.lang.NullPointerException
        at com.epam.labs.string.StringUtils.changeTo(StringUtils.java:68)
    */
    private static void demo3() {
        String oldStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String otherStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";

        StringTask str = new StringTask();
        str.setString(oldStr);

        String subStr = "ding";
        String replacement = null;

        System.out.println("original string: " + str.getString());
        System.out.println("size: " + str.stringSize());

        System.out.println("otherStr: " + otherStr);
        System.out.println("original.equals(otherStr): " + str.compareTo(otherStr));

        System.out.println("toLowerCase(): " + str.toLowerCase());
        System.out.println("toUpperCase(): " + str.toUpperCase());

        System.out.println("subStr: " + subStr);
        //System.out.println("receiveIndexes of subStr: " + StringUtils.arrayToString(str.receiveIndexes(subStr)));
        System.out.println("receiveIndexes of subStr: " + Arrays.toString(str.entries(subStr)));

        System.out.println("replacement: " + replacement);
        System.out.println("changed string: " + str.changeTo(subStr, replacement));

        System.out.println("found tokens : " + Arrays.toString(str.frequencyOfRepetitions()));
    }
}
