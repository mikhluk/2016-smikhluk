package com.epam.labs.stringrurenko.rurenkosrc;

/**
 * Created by Konstantin on 18.05.2016.
 */
public class Node {

    private String value;
    private int entries;

    public Node(String value, int entries) {
        this.value = value;
        this.entries = entries;
    }

    public String getValue() {
        return value;
    }

    public int getEntries() {
        return entries;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (getEntries() != node.getEntries()) return false;
        return getValue().equals(node.getValue());

    }

    @Override
    public int hashCode() {
        int result = getValue().hashCode();
        result = 31 * result + getEntries();
        return result;
    }

    @Override
    public String toString() {
        return  "Node{" +
                "value='" + value + '\'' +
                ", receiveIndexes=" + entries + '}'  ;
    }
}
