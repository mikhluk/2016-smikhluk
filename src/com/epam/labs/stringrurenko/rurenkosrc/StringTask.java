package com.epam.labs.stringrurenko.rurenkosrc;

import java.util.Arrays;

/**
 * Created by Comandante on 18.05.2016.
 */
public class StringTask implements StringUtils{

    private String string = "Dong-ding-dong";

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Override
    public int stringSize() {
        return string.length();
    }

    @Override
    public boolean compareTo(String str) {
        return string.equals(str);
    }

    @Override
    public String toLowerCase() {
        return string.toLowerCase();
    }

    @Override
    public String toUpperCase() {
        return string.toUpperCase();
    }

    @Override
    public int[] entries(String sub) {
        StringBuilder builder = new StringBuilder(string.toLowerCase());
        int[] array = new int[builder.length()];
        int quantity = 0;
        int position = 0;
        sub = sub.toLowerCase();

        while (builder.toString().contains(sub)){
            array[quantity++] = builder.indexOf(sub)+position;
            builder.delete(builder.indexOf(sub),builder.indexOf(sub)+sub.length());
            position += sub.length();
        }
        return Arrays.copyOf(array,quantity);
    }

    @Override
    public String changeTo(String stringToChange, String changeWith) {
        return string.replaceAll(stringToChange, changeWith);
    }

    @Override
    public Node[] frequencyOfRepetitions() {
        Node[] nodes = new Node[string.length()];
        int nodesQuantity = 0;

            String[] array = string.toLowerCase().split("-");
            int matches;

            for (String word : array){
                matches = 0;
                for (String item : array)
                    if (item.equals(word))
                        matches++;

                Node sample = new Node(word,matches);

                boolean allReadyHas = false;

                for (int i = 0; i < nodesQuantity; i++) {
                    if (nodes[i].equals(sample)){
                        allReadyHas = true;
                        break;
                    }
                }

                if (!allReadyHas){
                    nodes[nodesQuantity++] = sample;
                }
        }
        return Arrays.copyOf(nodes,nodesQuantity);
    }

}
