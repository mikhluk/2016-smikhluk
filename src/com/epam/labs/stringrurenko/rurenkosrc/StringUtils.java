package com.epam.labs.stringrurenko.rurenkosrc;

/**
 * Created by Konstantin on 18.05.2016.
 */
public interface StringUtils {

    int stringSize();
    boolean compareTo(String str);
    String toLowerCase();
    String toUpperCase();
    int[] entries(String of);
    String changeTo(String stringToChange, String changeWith);
    Node[] frequencyOfRepetitions();

}
