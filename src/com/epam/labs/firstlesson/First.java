package com.epam.labs.firstlesson;

/**
 * @author Sergey Mikhluk.
 */

public class First {
    public static void main(String[] args) {
        Aaa objPrint = new Aaa();
        PrinterClass obj1 = objPrint;
        obj1.printMethod();

        for (int i = 0; i < 5; ++i) {
            System.out.println(i);
        }

        Integer digit = new Integer(1);
        //digit.equals();
    }
}

class PrinterClass {
    public void printMethod() {
        System.out.println("Hello everyboody!");
    }
}

class Aaa extends PrinterClass {


    public void printMethod1() {
        System.out.println("Hello Aaa");
    }
}