package com.epam.labs.io.variant1;

import org.junit.Test;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.epam.labs.io.variant1.FileUtils.UNICODE_MODE;
import static org.junit.Assert.assertEquals;

/**
 * @author Sergey Mikhluk.
 */
public class FileUtilsTest {
    public static final Level LEVEL = Level.INFO;
    private static Logger logger = Logger.getLogger(FileUtilsTest.class.getName());

    public static final String FILE_1 = "d:/_testfile1.txt";
    public static final String FILE_2 = "d:/_testfile2.txt";
    public static final String FILE_3 = "d:/_testfile3.txt";

    @Test
    public void compareFilesTest() {
        logger.log(LEVEL, "compareFilesTest() invoked");
        Map<String, Long> result = FileUtils.compareFiles(FILE_1, FILE_2, null);

        Long expectedDifferences = 0L;
        assertEquals("mismatch at result.get(\"" + FileUtils.DIFFERENCES_QUANTITY + "\") ",
                expectedDifferences, result.get(FileUtils.DIFFERENCES_QUANTITY));

        logger.log(LEVEL, "compareFilesTest() exit");
    }

    @Test
    public void compareFilesTest1() {
        logger.log(LEVEL, "compareFilesTest1() invoked");
        Map<String, Long> result = FileUtils.compareFiles(FILE_1, FILE_3, null);

        Long expectedDifferences = 3L;
        Long expectedFirst = 0L;

        assertEquals("mismatch at result.get(\"" + FileUtils.DIFFERENCES_QUANTITY + "\") ",
                expectedDifferences, result.get(FileUtils.DIFFERENCES_QUANTITY));
        assertEquals("mismatch at result.get(\"" + FileUtils.FIRST_DIFFERENCE + "\") ",
                expectedFirst, result.get(FileUtils.FIRST_DIFFERENCE));

        logger.log(LEVEL, "compareFilesTest1() exit");
    }

    @Test
    public void compareFilesUnicodeTest() {
        logger.log(LEVEL, "compareFilesUnicodeTest() invoked");
        Map<String, Long> result = FileUtils.compareFiles(FILE_1, FILE_2, UNICODE_MODE);

        Long expectedDifferences = 0L;
        assertEquals("mismatch at result.get(\"" + FileUtils.DIFFERENCES_QUANTITY + "\") ",
                expectedDifferences, result.get(FileUtils.DIFFERENCES_QUANTITY));

        logger.log(LEVEL, "compareFilesUnicodeTest() exit");
    }

    @Test
    public void compareFilesUnicodeTest1() {
        logger.log(LEVEL, "compareFilesUnicodeTest1() invoked");
        Map<String, Long> result = FileUtils.compareFiles(FILE_1, FILE_3, UNICODE_MODE);

        Long expectedDifferences = 3L;
        Long expectedFirst = 0L;

        assertEquals("mismatch at result.get(\"" + FileUtils.DIFFERENCES_QUANTITY + "\") ",
                expectedDifferences, result.get(FileUtils.DIFFERENCES_QUANTITY));
        assertEquals("mismatch at result.get(\"" + FileUtils.FIRST_DIFFERENCE + "\") ",
                expectedFirst, result.get(FileUtils.FIRST_DIFFERENCE));

        logger.log(LEVEL, "compareFilesUnicodeTest1() exit");
    }
}