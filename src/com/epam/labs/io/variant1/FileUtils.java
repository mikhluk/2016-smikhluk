package com.epam.labs.io.variant1;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 1. Create an utility application that do a byte-to-byte comparison
 * content of two files. Print the result of comparison in next way:
 * - Result of comparison for files <file_name_1> <file_name_2>:
 * a) Result of application executing (if the differences were not found):
 * - the differences were not found
 * <p>
 * b) Result of application executing (if the differences were found):
 * - file sizes: <file_name_1> - Х Bytes <file_name_2> - Y Bytes
 * The first difference was found on : <byte_number>.
 * Total quantity of the different bytes - <quantity_of_different_bytes>
 * <p>
 * 2. Modify previous task by adding to the application a possibility
 * of comparison files in UNICODE format. For setting of comparison mode
 * use a parameter: /U
 *
 * @author Sergey Mikhluk.
 */
public class FileUtils {
    public static final String FIRST_DIFFERENCE = "first_difference";
    public static final String DIFFERENCES_QUANTITY = "differences_quantity";
    public static final String LENGTH_1 = "length1";
    public static final String LENGTH_2 = "length2";
    public static final String UNICODE_MODE = "/U";

    public static Map<String, Long> compareFiles(String file1, String file2, String mode) {
        if (file1 == null || file2 == null) {
            throw new NullPointerException();
        }
        File f1 = new File(file1);
        File f2 = new File(file2);

        Map<String, Long> result = new HashMap<>(2);
        result.put(FIRST_DIFFERENCE, 0L);
        result.put(DIFFERENCES_QUANTITY, 0L);
        result.put(LENGTH_1, f1.length());
        result.put(LENGTH_2, f2.length());

        if (UNICODE_MODE.equals(mode)) {
            result = unicodeModeCompare(f1, f2, result);
        } else {
            result = byteModeCompare(f1, f2, result);
        }
        return result;
    }

    private static Map<String, Long> byteModeCompare(File f1, File f2, Map<String, Long> result) {
        long differencesQuantity = 0;

        try (BufferedInputStream buffer1 = new BufferedInputStream(new FileInputStream(f1));
             BufferedInputStream buffer2 = new BufferedInputStream(new FileInputStream(f2))) {
            for (long position = 0; position < Math.max(f1.length(), f2.length()); position++) {
                if (buffer1.read() != buffer2.read()) {
                    if (differencesQuantity == 0) {
                        result.put(FIRST_DIFFERENCE, position);
                    }
                    differencesQuantity++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.put(DIFFERENCES_QUANTITY, differencesQuantity);
        return result;
    }

    private static Map<String, Long> unicodeModeCompare(File f1, File f2, Map<String, Long> result) {
        long differencesQuantity = 0;

        try (BufferedReader bis1 = new BufferedReader(new FileReader(f1));
             BufferedReader bis2 = new BufferedReader(new FileReader(f2))) {
            for (long position = 0; position < Math.max(f1.length(), f2.length()); position++) {
                if (bis1.read() != bis2.read()) {
                    if (differencesQuantity == 0) {
                        result.put(FIRST_DIFFERENCE, position);
                    }
                    differencesQuantity++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        result.put(DIFFERENCES_QUANTITY, differencesQuantity);
        return result;
    }
}