package com.epam.labs.io.variant1;

import java.util.Map;

import static com.epam.labs.io.variant1.FileUtils.*;

/**
 * 1. Create an utility application that do a byte-to-byte comparison
 * content of two files. Print the result of comparison in next way:
 * - Result of comparison for files <file_name_1> <file_name_2>:
 * a) Result of application executing (if the differences were not found):
 * - the differences were not found
 * <p>
 * b) Result of application executing (if the differences were found):
 * - file sizes: <file_name_1> - Х Bytes <file_name_2> - Y Bytes
 * The first difference was found on : <byte_number>.
 * Total quantity of the different bytes - <quantity_of_different_bytes>
 * <p>
 * 2. Modify previous task by adding to the application a possibility
 * of comparison files in UNICODE format. For setting of comparison mode
 * use a parameter: /U
 *
 * Attention! Before running an application, please, copy files from
 * the path src.com.epam.io.resources to d:\
 *
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static final String FILE_1 = "d:/_testfile1.txt";
    public static final String FILE_2 = "d:/_testfile2.txt";
    public static final String FILE_3 = "d:/_testfile3.txt";

    public static void main(String[] args) {
        /* Expected result:
            File 1 = d:/_testfile1.txt, File 2 = d:/_testfile2.txt
            1. Byte-format comparison:
                The differences were not found.
            2. UNICODE-format comparison:
                The differences were not found.
        */
        demo(FILE_1, FILE_2);

        /* Expected result:
        File 1 = d:/_testfile1.txt, File 2 = d:/_testfile3.txt
        1. Byte-format comparison:
            Size of file 1: <22> bytes.
            Size of file 2: <24> bytes.
            The first differences was found at: <0> byte.
            Total quantity of different bytes: <3>.
        2. UNICODE-format comparison:
            Size of file 1: <22> bytes.
            Size of file 2: <24> bytes.
            The first differences was found at: <0> byte.
            Total quantity of different bytes: <3>.
        */
        demo(FILE_1, FILE_3);
    }

    private static void demo(String file1, String file2) {
        System.out.println("\nFile 1 = " + file1 + ", File 2 = " + file2);
        Map<String, Long> result;
        result = FileUtils.compareFiles(file1, file2, null);

        System.out.println("1. Byte-format comparison:");
        printResult(result);

        result = FileUtils.compareFiles(file1, file2, UNICODE_MODE);
        System.out.println("2. UNICODE-format comparison:");
        printResult(result);
    }

    private static void printResult(Map<String, Long> result) {
        if (result.get(DIFFERENCES_QUANTITY) == 0) {
            System.out.println("    The differences were not found.");
        } else {
            System.out.println("    Size of file 1: <" + result.get(LENGTH_1) + "> bytes.");
            System.out.println("    Size of file 2: <" + result.get(LENGTH_2) + "> bytes.");
            System.out.println("    The first differences was found at: <" + result.get(FIRST_DIFFERENCE) + "> byte.");
            System.out.println("    Total quantity of different bytes: <" + result.get(DIFFERENCES_QUANTITY) + ">.");
        }
    }
}
