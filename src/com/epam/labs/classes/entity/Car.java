package com.epam.labs.classes.entity;

public class Car {
    private int id;
    private String brand;
    private String model;
    private int releaseYear;
    private EColor color;
    private double price;
    private int registrationNumber;

    public Car() {
    }

    public Car(int id, String brand, String model, int releaseYear, EColor color, double price, int registrationNumber) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.releaseYear = releaseYear;
        this.color = color;
        this.price = price;
        this.registrationNumber = registrationNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public EColor getColor() {
        return color;
    }

    public void setColor(EColor color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id = " + id +
                ", brand = \"" + brand + "\"" +
                ", model = \"" + model + "\"" +
                ", releaseYear = " + releaseYear +
                ", color = " + color +
                ", price = " + price +
                ", registrationNumber = " + registrationNumber + "}";
    }
}
