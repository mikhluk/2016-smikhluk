package com.epam.labs.classes.entity;

/**
 * @author Sergey Mikhluk.
 */
public enum EColor {
    GREEN, YELLOW, RED
}
