package com.epam.labs.classes;

import com.epam.labs.classes.entity.Car;

/**
 * Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
 * 1. Создать массив объектов Car.
 * 2. Вывести:
 * a) список автомобилей заданной марки;
 * b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
 * c) список автомобилей заданного года выпуска, цена которых больше указанной.
 *
 * @author Sergey Mikhluk.
 */
public class CarRunner {
    public static final int RELEASE_YEAR = 2006;
    public static final int PRICE = 60_000;
    public static final int EXPLOITED_YEARS = 15;
    public static final String MODEL = "M3";
    public static final String BRAND= "BMW";
    public static final int CURRENT_YEAR = 2016;

    public static void main(String[] args) {
        CarDAO carDAO = new CarDAO();

        System.out.println("Список всех машин:");
        for (Car car : carDAO.getAllCars()) {
            System.out.println(car);
        }

        System.out.println("\nСписок машин марки " + BRAND);
        for (Car car : carDAO.getCarsByBrand(BRAND)) {
            if (car != null) {
                System.out.println(car);
            }
        }


        System.out.println("\nСписок машин модели " + MODEL + " эксплуатируются больше " + EXPLOITED_YEARS + " лет:");
        for (Car car : carDAO.getCarsByExploitedYears(MODEL, CURRENT_YEAR - EXPLOITED_YEARS)) {
            if (car != null) {
                System.out.println(car);
            }
        }

        System.out.println("\nСписок машин старше " + RELEASE_YEAR + " Ценой больше " + PRICE + ":");
        for (Car car : carDAO.getCarsByYearAndPrice(RELEASE_YEAR, PRICE)) {
            if (car != null) {
                System.out.println(car);
            }
        }
    }
}
