package com.epam.labs.classes;

import com.epam.labs.classes.entity.Car;

/**
 * @author Sergey Mikhluk.
 */
public class CarDAO {
    Car[] cars;
    int size;

    public CarDAO() {
        cars = CarFactory.createCarArray();
        size = cars.length;
    }

    /**
     * Creates and returns array of cars filtered by given brand
     * @param   brand the filter
     * @return  array of Car object filtered by given brand
     */
    public Car[] getCarsByBrand(String brand) {
        Car[] carArray = new Car[size];
        int j = 0;

        for (Car car : cars) {
            if (brand.equals(car.getBrand())) {
                carArray[j++] = car;
            }
        }
        return carArray;
    }


    /**
     * Creates and returns array of cars filtered by model and by exploited years
     * @param   model the filter
     * @param   releaseYear the filter
     * @return  array of Car object filtered by given model and by given year of release
     */
    public Car[] getCarsByExploitedYears(String model, int releaseYear) {
        Car[] carArray = new Car[size];
        int j = 0;

        for (Car car : cars) {
            if (model.equals(car.getModel()) && car.getReleaseYear() <= releaseYear) {
                carArray[j++] = car;
            }
        }
        return carArray;
    }

    /**
     * Creates and returns array of cars filtered by price and by year of release
     * @param   releaseYear filter
     * @param   price filter
     * @return  array of Car object filtered by given price and by given year of release
     */
    public Car[] getCarsByYearAndPrice(int releaseYear, double price) {
        Car[] carArray = new Car[size];

        int j = 0;
        for (int i = 0; i < size; i++) {
            if (cars[i].getReleaseYear() >= releaseYear && cars[i].getPrice() >= price) {
                carArray[j++] = cars[i];
            }
        }
        return carArray;
    }

    /**
     *
     * @return  array of Car without any filters
     */
    public Car[] getAllCars() {
        return cars;
    }
}
