package com.epam.labs.classes;

import com.epam.labs.classes.entity.Car;
import com.epam.labs.classes.entity.EColor;

/**
 * @author Sergey Mikhluk.
 */

public class CarFactory {
    public static final int SIZE = 7;

	public static Car[] createCarArray() {
		int index = 0;
        Car[] cars = new Car[SIZE];

        cars[index++] = new Car(1, "BMW", "M3", 1998, EColor.GREEN, 61000.0, 96_325_874);
        cars[index++] = new Car(2, "BMW", "M3", 2002, EColor.RED, 68000.0, 98_743_625);
        cars[index++] = new Car(3, "BMW", "X5", 2003, EColor.YELLOW, 31000.0, 74_991_254);
        cars[index++] = new Car(4, "Audi", "Q6", 2006, EColor.RED, 75000.0, 87_451_323);
        cars[index++] = new Car(5, "Audi", "Q7", 2008, EColor.GREEN, 86000.0, 89_746_531);
        cars[index++] = new Car(6, "Mersedes-Benz", "SLK", 2009, EColor.YELLOW, 65000.0, 96_325_874);
        cars[index++] = new Car(7, "Mersedes-Benz", "McLaren", 2012, EColor.RED, 59000.0, 41_321_654);

		return cars;
	}
}
