package com.epam.labs.collections;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleLRUCacheTest {
    public static void main(String[] args) {
        SimpleLRUCache<String, Integer> simpleLRUCache = new SimpleLRUCache(2);

        simpleLRUCache.put("1", 1);
        simpleLRUCache.put("2", 2);
        simpleLRUCache.put("3", 3);

        simpleLRUCache.get("2");

        simpleLRUCache.put("9", 9);
        System.out.println(simpleLRUCache);  // {2, 9} todo почему?
    }
}
