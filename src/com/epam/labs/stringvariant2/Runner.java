package com.epam.labs.stringvariant2;

import java.util.Arrays;

/**
 * Создайте программу, которая преобразовывает в строке "Dong-ding-dong"
 * - выводит количество букв в строке +
 * - сравнивает данную строку с любой другой, игнорируя регистр
 * - все буквы к верхнему/нижнему регисту и выводит результат на консоль
 * - выводит на консоль все индексы слова "dong"
 * - заменяет каждое вхождение слова dong на bong
 * - находит все одинаковые слова, подсчитывая их количество
 *
 * @author Sergey Mikhluk.
 */
public class Runner {

    public static void main(String[] args) {
        demo1();
        //demo2();
        //demo3();
    }

    private static void demo(String oldStr, String otherStr, String subStr, String replacement) {
        System.out.println("original string: " + oldStr);
        System.out.println("size: " + UtilsStatic.stringSize(oldStr));

        System.out.println("otherStr: " + otherStr);
        System.out.println("original.equals(otherStr): " + UtilsStatic.compareTo(oldStr, otherStr));

        System.out.println("toLowerCase(): " + UtilsStatic.toLowerCase(oldStr));
        System.out.println("toUpperCase(): " + UtilsStatic.toUpperCase(oldStr));

        System.out.println("subStr: " + subStr);
        System.out.println("indexes of subStr: " + Arrays.toString(UtilsStatic.receiveIndexes(oldStr, subStr)));

        System.out.println("replacement: " + replacement);
        System.out.println("changed string: " + UtilsStatic.changeTo(oldStr, subStr, replacement));

        System.out.println("found tokens : " + Arrays.toString(UtilsStatic.getDuplicates(oldStr)));
    }

    /**
     * Expected result:
     * original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * size: 45
     * otherStr: DING-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * original.equals(otherStr): true
     * toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
     * toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
     * subStr: ding
     * ignoreCase = true
     * indexes of subStr: [0, 5, 21, 27]
     * replacement: bONg
     * case sensitive = true
     * changed string: bONg-diNg-DOng-di ng-bONg--bONg!ng?di(di)(di)
     * found tokens : [{value = ding, receiveIndexes = 3}, {value = diNg, receiveIndexes = 1}, {value = DOng, receiveIndexes = 1}, {value = di, receiveIndexes = 4}, {value = ng, receiveIndexes = 2}]
     */
    private static void demo1() {
        String oldStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        //String oldStr = ";.,sad=[]()DoNG-      -dOng-dINg-dong@#$@#$#dong214dongAs";
        String otherStr = "DING-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subStr = "ding";
        //String replacement = "bong";
        String replacement = "bONg";

        System.out.println("DEMO 1.");
        demo(oldStr, otherStr, subStr, replacement);
    }

    /**
     * Expected result:
     * original string:
     * size: 0
     * otherStr: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * original.equals(otherStr): false
     * toLowerCase():
     * toUpperCase():
     * subStr:
     * receiveIndexes of subStr: {-1}
     * replacement: bong
     * changed string:
     * found tokens : []
     */
    private static void demo2() {
        //String oldStr = null;
        String oldStr = "";
        //String otherStr = "";
        String otherStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subStr = "";
        String replacement = "bong";

        System.out.println("\nDEMO 2.");
        demo(oldStr, otherStr, subStr, replacement);
    }

    /**
     * Expected result:
     * original string: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * size: 45
     * otherStr: ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)
     * original.equals(otherStr): true
     * toLowerCase(): ding-ding-dong-di ng-ding--ding!ng?di(di)(di)
     * toUpperCase(): DING-DING-DONG-DI NG-DING--DING!NG?DI(DI)(DI)
     * subStr: ding
     * receiveIndexes of subStr: {0, 5, 21, 27}
     * replacement: null
     * java.lang.NullPointerException
     * at com.epam.labs.string.StringUtils.changeTo(StringUtils.java:68)
     */
    private static void demo3() {
        String oldStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String otherStr = "ding-diNg-DOng-di ng-ding--ding!ng?di(di)(di)";
        String subStr = "ding";
        //String replacement = null;
        String replacement = "";

        System.out.println("\nDEMO 3.");
        demo(oldStr, otherStr, subStr, replacement);
    }
}
