package com.epam.labs.stringvariant2;

import com.epam.labs.string.Node;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Вариант реализации через статический класс.
 * <p>
 * Создайте программу, которая преобразовывает в строке "Dong-ding-dong"
 * - выводит количество букв в строке +
 * - сравнивает данную строку с любой другой, игнорируя регистр
 * - все буквы к верхнему/нижнему регисту и выводит результат на консоль
 * - выводит на консоль все индексы слова "dong"
 * - заменяет каждое вхождение слова dong на bong
 * - находит все одинаковые слова, подсчитывая их количество
 *
 * @author Sergey Mikhluk.
 */
public class UtilsStatic {

    public static int stringSize(String str) throws NullPointerException {
        // ms: лучше писать максимально просто и похоже, чтобы все было в одном стиле
        // минимальность кода, как можно меньше, если код есть, то выполняет какую-то функцию без нее нельзя

        return str.length();
    }

    // сравнивает данную строку с любой другой, игнорируя регистр
    public static boolean compareTo(String string1, String string2) {
        if (string1 == null || string2 == null) {
            throw new NullPointerException();
        }
        int size = stringSize(string1);
        if (size != stringSize(string2)) {
            return false;
        }

        char char1;
        char char2;
        for (int i = 0; i < size; i++) {
            char1 = string1.charAt(i);
            char2 = string2.charAt(i);

            if (char1 != char2) {
                char1 = Character.toLowerCase(char1);
                char2 = Character.toLowerCase(char2);

                if (char1 != char2) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String toLowerCase(String string) {
        if (string == null) {
            throw new NullPointerException();
        }

        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toLowerCase(chars[i]);
        }
        return String.valueOf(chars);
    }

    public static String toUpperCase(String string) {
        if (string == null) {
            throw new NullPointerException();
        }

        char[] chars = string.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toUpperCase(chars[i]);
        }
        return String.valueOf(chars);
    }

    public static int[] receiveIndexes(String string, String subString) {
        boolean ignoreCase = true;
        if (string == null) {
            throw new NullPointerException("Null parameter 'string'!");
        }
        if (subString == null || subString.equals("")) {
            throw new IllegalArgumentException("Null or empty parameter subString!");
        }

        if (ignoreCase) {
            string = toLowerCase(string);
            subString = toLowerCase(subString);
        }

        int[] indexes = new int[stringSize(string)];
        indexes[0] = -1;

        int entriesCount = 0;
        int index = string.indexOf(subString);
        while (index >= 0) {
            indexes[entriesCount] = index;
            index = string.indexOf(subString, index + 1);
            entriesCount++;
        }

        int[] indexesResult = new int[entriesCount];
        System.arraycopy(indexes, 0, indexesResult, 0, entriesCount);
        System.out.println("ignoreCase = " + ignoreCase);

        return indexesResult;
    }

    public static String changeTo(String string, String subString, String replacement) {
        if (string == null) {
            throw new NullPointerException("Null parameter 'string'!");
        }
        if (replacement == null) {
            throw new NullPointerException("Null parameter 'replacement'!");
        }
        if (subString == null || subString.equals("")) {
            return string;
        }

        System.out.println("case sensitive = true");
        return string.replace(subString, replacement);
    }


    public static Node[] getDuplicatesOld(String string) {

        String delimiter = " \"(),+-=<>?!@#$%^&*~'./\\";
        StringTokenizer tokenizer = new StringTokenizer(string, delimiter);
        Node[] nodes = new Node[tokenizer.countTokens()];

        int i = 0;
        int size = 0;
        String token;
        boolean foundEntry;

        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();
            foundEntry = false;
            for (int j = 0; j < size; j++) {
                if (nodes[j].getValue().equals(token)) {
                    nodes[j].setQuantity(nodes[j].getQuantity() + 1);
                    foundEntry = true;
                }
            }

            if (!foundEntry) {
                nodes[i++] = new Node(token, 1);
                size++;
            }
        }

        return nodes;
    }

    public static Node[] getDuplicates(String string) {
        if (string == null) {
            throw new NullPointerException("Null parameter 'string'!");
        }
        if (string.equals("")) {
            return new Node[]{new Node("", 0)};
        }

        boolean ignoreCase = true;

        Node[] nodes = new Node[stringSize(string)];
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(string);

        boolean duplicateFound;
        int nodesSize = 0;
        int i = 0;
        String token;
        String value;

        while (m.find()) {   // ms  одним циклом, по ключу или по значению массива, аскии трансформировать в int
            //System.out.println(text.substring(m.start(), m.end())+ "*");
            token = ignoreCase ? toLowerCase(m.group()) : m.group();
            duplicateFound = false;
            for (int j = 0; j < nodesSize; j++) {
                value = ignoreCase ? toLowerCase(nodes[j].getValue()) : nodes[j].getValue();
                if (token.equals(value)) {
                    nodes[j].setQuantity(nodes[j].getQuantity() + 1);
                    duplicateFound = true;
                }
                //System.out.println(token);
            }

            if (!duplicateFound) {
                nodes[i++] = new Node(token, 1);
                nodesSize++;
            }
        }

        Node[] nodesResult = new Node[nodesSize];
        System.arraycopy(nodes, 0, nodesResult, 0, nodesSize);

        return nodesResult;
    }

}
