package important.inner.question5;

/**
 * what is the result?
 * a) x is 24  !!!
 * b) x is 0
 * c) Compiler error 7
 * d) Compiler error 9
 * e) Compiler error 12
 *
 * @author Sergey Mikhluk.
 */
public class Outer {   // 1
    private int x = 24;

    public int getX() {
        String message = "x is ";
        class Inner {
            private int x = Outer.this.x;  // 7
            public void printX() {
                System.out.println(message + x);  // 9
            }
        }
        Inner in = new Inner();  // 12
        in.printX();
        return x;
    }
}

class RunOuter {
    public static void main(String[] args) {
        new Outer().getX();
    }
}
