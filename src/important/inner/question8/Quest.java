package important.inner.question8;

/**
 * what is the result?
 * a) inner  !!!
 * b) inner inner
 * c) inner inner inner
 * d) compile error
 * e) runtime error

 * @author Sergey Mikhluk.
 */
abstract class Abstract {
    abstract Abstract meth();
}
class Owner {
    Abstract meth(){
        class Inner extends Abstract {
            Abstract meth(){
                System.out.println("inner ");
                return new Inner();
            }
        }
        return new Inner();
    }
}

public abstract class Quest extends Abstract{
    public static void main(String[] args) {
        Owner ob = new Owner();
        Abstract abs = ob.meth();
        abs.meth();
    }
}
