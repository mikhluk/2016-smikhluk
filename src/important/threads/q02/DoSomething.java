package important.threads.q02;

/**
 * @author Sergey Mikhluk.
 */
public class DoSomething implements Runnable {
    @Override
    public void run() {
        System.out.print("Do something");
    }

    public static void main(String[] args) throws InterruptedException {
        DoSomething r = new DoSomething();
        Thread t = new Thread(r);
        t.start();
        t.join();
        System.out.println(" else");   // Do something else
    }

}

/*
A. Do something else
B. else Do something
C. else
D. Do something
E. The output is indeterminate.

2. A. The call to join on line 7 of Main causes the main thread to wait until t is done
executing. The t thread prints Do something and then ends, and line 8 prints else.
Therefore, the answer is A.

 */