package important.threads.q13;

/**
 * @author Sergey Mikhluk.
 */
public class Q13 {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("do something");

            }
        });
        t.start();  // 10
    }
}

/*
what is the state of the thread t immediately after line 10 executes?
A. NEW
B. RUNNABLE
C. BLOCKED
D. TERMINATED
E. The state of the thread is indeterminate.

13. B. The state of t is NEW after it is instantiated on line 9, and t becomes
RUNNABLE once it is started. Therefore, the answer is B.


 */
