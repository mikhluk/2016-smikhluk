package important.threads.task03;

/**
 * @author Sergey Mikhluk.
 */
class T1 implements Runnable{
    @Override
    public void run() {
        System.out.println("t1 ");
    }
}

class T2 extends Thread{
    @Override
    public void run() {
        System.out.println("t2 ");
    }
}

public class Quest3 {
    public static void main(String[] args) {
        T1 t1 = new T1();
//        T2  t2 = new T2(t1);
        //Error:(23, 18) java: constructor T2 in class important.threads.task03.T2 cannot be applied to given types;
        //reason: actual and formal argument lists differ in length
//
//        t1.start();
        // Error:(25, 11) java: cannot find symbol method start()
        // location: variable t1 of type important.threads.task03.T1

//        t2.start();
    }

}