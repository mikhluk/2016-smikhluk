package important.threads.q03;

/**
 * @author Sergey Mikhluk.
 */
public class PrintB {
    public static void main(String[] args) {
        Thread a = new PrintA();
        a.run();  //4
        System.out.println("B");
    }
}

/*
A. The program generates an exception at runtime. -
B. The program does not compile. -
C. The output varies and is either AB or BA .
D. The output is always AB . +

3. D. The code compiles fine and runs fine, so A and B are incorrect. On line 4 of the
PrintB class, the run method of the new thread is invoked. However, the run method
does not start a new thread in the process. (Only a call to start starts a new thread.)
In other words, this program is not multithreaded and the call to run occurs within the
main thread. The output of this program is always AB and therefore the answer is D.
 */
