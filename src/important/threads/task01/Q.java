package important.threads.task01;

/**
 * @author Sergey Mikhluk.
 */
public class Q implements Runnable{
    int i = 0;
    @Override
    public void run() {   // public int run() {
        System.out.println("i = " + ++i);
        //return i;
    }
}
