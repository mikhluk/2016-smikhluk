6. What state can a WAITING thread transition into? (Select one.)
A. NEW
B. RUNNABLE
C. BLOCKED +
D. TIMED_WAITING
E. TERMINATED


6. C. When a WAITING thread receives a notify or notifyAll, it transitions into the BLOCKED
state because the monitor lock it needs is not available. (The thread that called notify or
notifyAll owns it.) Therefore, the answer is C.


7. If the state of a thread is BLOCKED , what must its previous state have been? (Select all that
apply.)
A. NEW
B. RUNNABLE +
C. WAITING +
D. TIMED_WAITING +
E. TERMINATED


7. B, C, and D. A NEW thread can only transition into the RUNNABLE state, so A is incorrect. A
RUNNABLE thread transitions into BLOCKED when attempting to acquire an unavailable monitor
lock, so B is correct. A WAITING or TIMED_WAITING thread transitions into BLOCKED on
a notify or notifyAll, so C and D are correct. The state of a TERMINATED thread never
changes, so E is incorrect. Therefore, the answers are B, C, and D.


8. Fill in the blanks: A thread that invokes "wait()" is a ___ and a thread
that invokes "notify" or "notifyAll" is a ____.

8. consumer, producer. The wait and notify methods are used in producer/consumer models.
If a consumer thread has nothing to consume, it waits. When a producer thread produces,
it notifies. Therefore, a thread that invokes wait is a consumer and a thread that invokes
notify or notifyAll is a producer.


