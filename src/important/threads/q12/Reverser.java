package important.threads.q12;

/**
 * @author Sergey Mikhluk.
 */
public class Reverser extends Thread{
    private StringBuffer sb;

    public Reverser(StringBuffer sb){
        this.sb = sb;
    }

    public void run(){
        sb.reverse();
    }

    public static void main(String[] args) {
        StringBuffer s = new StringBuffer("xyz");
        Reverser r = new Reverser(s);
        r.start();
        System.out.println(s);
        r.start();  //17
        System.out.println(s);
    }
}
/*
A. xyzzyx
B. zyxxyz
C. Either xyzzyx or zyxxyz
+ D. The code generates an exception at runtime.
E. The code does not compile.

12. D. You cannot start a thread twice. Line 17 compiles, but the r thread has already been
started, so line 17 generates an IllegalThreadStateException. Therefore, the answer is D.

 */