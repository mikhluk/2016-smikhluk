package important.threads.task05;

/**
 * @author Sergey Mikhluk.
 */
class Quest5 extends Thread {
    Quest5() {
    }
    Quest5(Runnable r) {
        super(r);
    }

    public void run(){
        System.out.println("thread");
    }

    public static void main(String[] args) {
        Runnable r = new Quest5();  // вызовется конструктор по умолчанию, run() не вызовется
        Quest5 t = new Quest5(r);
        t.start();   // thread
    }

}
