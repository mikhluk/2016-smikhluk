package important.threads.q05;

/**
 * @author Sergey Mikhluk.
 */
public class PrintX {
    public static void main(String[] args) {
        MyTarget target = new MyTarget();
//        Thread t = new Thread(target);  //4  target does not implement Runnable
//
//        t.start();
        System.out.println("y");

    }
}
/*
A. xxxxxxxxxxy
B. yxxxxxxxxxx
C. Ten x s and one y printed in an indeterminate order.
D. xxxxxxxxxxy or yxxxxxxxxxx
E. The code does not compile. +

5. E. The MyTarget class does not implement Runnable, so line 4 of PrintX does not
compile. Therefore, the answer is E.
 */