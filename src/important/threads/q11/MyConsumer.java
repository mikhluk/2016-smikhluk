package important.threads.q11;

/**
 * @author Sergey Mikhluk.
 */
public class MyConsumer extends Thread{
    private StringBuffer sb;

    public MyConsumer(StringBuffer sb){
        this.sb = sb;
    }

    public void run(){
        if(sb!= null && sb.length() == 0){
            try {
                System.out.println("Waiting");
                sb.wait();    // 11
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sb.reverse();
        }
    }

}
