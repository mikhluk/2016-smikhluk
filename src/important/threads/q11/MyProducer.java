package important.threads.q11;

/**
 * @author Sergey Mikhluk.
 */
public class MyProducer {
    public static void main(String[] args){
        StringBuffer sb = new StringBuffer("");
        MyConsumer consumer = new MyConsumer(sb);
        consumer.start();
        Thread.yield();
        sb.append("abc");
        synchronized (sb){
            sb.notifyAll();
        }
        System.out.println(sb);
    }
}

/*
A. The output is Waiting following by cba .
B. The output is abc .
C. Either A or B always occurs.
D. The code may generate an exception at runtime.
E. The code does not compile.


11. D. The code compiles fine, so E is incorrect. A thread cannot invoke wait on an object
unless that thread owns the object’s monitor lock. In other words, a call to wait must
appear within a synchronized method or block of code. Line 11 of the MyConsumer class
generates an IllegalMonitorStateException at runtime because the thread does not own
the lock of sb. Therefore, the answer is D.

 */