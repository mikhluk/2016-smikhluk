package important.threads.q15;

/**
 * @author Sergey Mikhluk.
 */
public class Calendar {
    private static int FIRST_DAY = 1;
    public static synchronized void setFirstDay(int value){
        FIRST_DAY = value;
    }
    public static int getFirstDay(){
        return FIRST_DAY;
    }
}

/*
which of the following statements is true? (Select one)
A. The code does not compile.
B. Invoking setFirstDay generates an exception at runtime.
+ C. A thread that enters the setFirstDay method must own the lock of the Calendar ’ s
Class object.
D. A thread that enters the setFirstDay method must own the lock of any instance of
Calendar .
E. The getFirstDay method must also be synchronized.

15. C. The code compiles and runs fine, so A and B are incorrect. E is false; there is no such
requirement of the getFirstDay method. D is also false; owning the lock of a particular
Calendar object is not sufficient for entering setFirstDay. The thread that enters
setFirstDay must own the lock of the Class object of Calendar. Therefore, the answer is C.
 */