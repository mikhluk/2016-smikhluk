package important.threads.q04;

/**
 * @author Sergey Mikhluk.
 */
public class PrintSomething implements Runnable {
    private String value;
    public PrintSomething(String value){
        this.value = value;
    }
    @Override
    public void run() {
        try {
            Thread.sleep((int)(Math.random()*4000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(value);
    }

    public static void main(String[] args) {
        Runnable x = new PrintSomething("x");  //16
        Runnable y = new PrintSomething("y");  //17

        Thread one = new Thread(x); //18
        Thread two = new Thread(y);//19
        two.start();
        one.start();
    }
}

/*
A. The output is always xy .
B. The output is always yx .
C. The output can be either xy or yx . +
D. Lines 16 and 17 generate compiler errors.
E. Lines 18 and 19 generate compiler errors.

4 The code compiles fine, so D and E are incorrect. The order of the output of these
two threads is indeterminate because they are scheduled by the JVM and there is no
guarantee of the order in which they will execute (even if the call to sleep did not occur
in the run method). Therefore, the output can either be xy or yx and the answer is C.

 */
