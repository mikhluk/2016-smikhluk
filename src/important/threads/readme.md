Answers to Review Questions

1. C. The thread t is instantiated but has not been started yet. That is the defi nition of a
new thread, so its state is NEW and the answer is C.

2. A. The call to join on line 7 of Main causes the main thread to wait until t is done
executing. The t thread prints Do something and then ends, and line 8 prints else.
Therefore, the answer is A.

3. D. The code compiles fi ne and runs fi ne, so A and B are incorrect. On line 4 of the
PrintB class, the run method of the new thread is invoked. However, the run method
does not start a new thread in the process. (Only a call to start starts a new thread.)
In other words, this program is not multithreaded and the call to run occurs within the
main thread. The output of this program is always AB and therefore the answer is D.

4 The code compiles fi ne, so D and E are incorrect. The order of the output of these
two threads is indeterminate because they are scheduled by the JVM and there is no
guarantee of the order in which they will execute (even if the call to sleep did not occur
in the run method). Therefore, the output can either be xy or yx and the answer is C.

5. E. The MyTarget class does not implement Runnable, so line 4 of PrintX does not
compile. Therefore, the answer is E.

6. C. When a WAITING thread receives a notify or notifyAll, it transitions into the BLOCKED
state because the monitor lock it needs is not available. (The thread that called notify or
notifyAll owns it.) Therefore, the answer is C.

7. B, C, and D. A NEW thread can only transition into the RUNNABLE state, so A is incorrect. A
RUNNABLE thread transitions into BLOCKED when attempting to acquire an unavailable monitor
lock, so B is correct. A WAITING or TIMED_WAITING thread transitions into BLOCKED on
a notify or notifyAll, so C and D are correct. The state of a TERMINATED thread never
changes, so E is incorrect. Therefore, the answers are B, C, and D.

8. consumer, producer. The wait and notify methods are used in producer/consumer models.
If a consumer thread has nothing to consume, it waits. When a producer thread produces,
it notifi es. Therefore, a thread that invokes wait is a consumer and a thread that invokes
notify or notifyAll is a producer.

9. A and D. The code compiles fi ne, so C and E are incorrect. Synchronized methods attempt to
acquire the lock on the this reference, so A is true. B is false; a thread can invoke deposit
without the lock. If the thread does not have the lock, it will have to acquire it. D is true;
the getBalance method is not synchronized, so it is possible to invoke getBalance while
another thread is in the middle of a deposit or withdraw. Therefore, the answers are A and D.

10. A. A static method can be declared as synchronized. The this reference it is attempting to
acquire is to the Class object of the Paint class and not an actual instance of Paint. The
code compiles fi ne. Because the color fi eld is private and the only way to access it is through
synchronized methods, it is protected from concurrent access problems and the answer is A.

11. D. The code compiles fi ne, so E is incorrect. A thread cannot invoke wait on an object
unless that thread owns the object’s monitor lock. In other words, a call to wait must
appear within a synchronized method or block of code. Line 11 of the MyConsumer class
generates an IllegalMonitorStateException at runtime because the thread does not own
the lock of sb. Therefore, the answer is D.

12. D. You cannot start a thread twice. Line 17 compiles, but the r thread has already been
started, so line 17 generates an IllegalThreadStateException. Therefore, the answer is D.

13. B. The state of t is NEW after it is instantiated on line 9, and t becomes
RUNNABLE once it is started. Therefore, the answer is B.

14. A. This question is tricky. The state of t is NEW after it is instantiated on line 7. Because t is
never started, it does not change states. The sleep method is static and causes the current
thread to sleep, not the thread t. Therefore, the answer is A.

15. C. The code compiles and runs fi ne, so A and B are incorrect. E is false; there is no such
requirement of the getFirstDay method. D is also false; owning the lock of a particular
Calendar object is not suffi cient for entering setFirstDay . The thread that enters
setFirstDay must own the lock of the Class object of Calendar . Therefore, the answer is C.