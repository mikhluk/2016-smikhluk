package important.threads.q14;

/**
 * @author Sergey Mikhluk.
 */
public class Q14 {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {  //7
            @Override
            public void run() {
                System.out.println("do something");
            }
        });
        try {
            t.sleep(1000);  // 13
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
/*
what is the state of the thread t immediately after line 13 executes?
A. NEW
B. RUNNABLE
C. TIMED_WAITING
D. WAITING
E. The state of the thread is indeterminate.

14. A. This question is tricky. The state of t is NEW after it is instantiated on line 7. Because t is
never started, it does not change states. The sleep method is static and causes the current
thread to sleep, not the thread t. Therefore, the answer is A.

 */
