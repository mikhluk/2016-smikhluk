package important.threads.q10;

/**
 * @author Sergey Mikhluk.
 */
public class Paint {
    private static int color = 1;  // 2

    public synchronized static void setColor(int newColor) {  // 4
        color = newColor;
    }

    public synchronized static int getColor(){  // 9
        return color;
    }
}

/*
+ A. The color field is protected from concurrent access problems.
B. Line 2 generates a compiler error.
C. Lines 4 and 9 generate compiler errors.
D. Lines 6 and 10 generate compiler errors.

10. A. A static method can be declared as synchronized. The this reference it is attempting to
acquire is to the Class object of the Paint class and not an actual instance of Paint. The
code compiles fine. Because the color field is private and the only way to access it is through
synchronized methods, it is protected from concurrent access problems and the answer is A.

 */