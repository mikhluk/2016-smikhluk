package important.threads.q09;

/**
 * @author Sergey Mikhluk.
 */
public class BankAccount {
    private double balance;
    public synchronized void deposit(double amount){ // 4
        balance += amount;
    }

    public void withdraw(double amount){
        synchronized (this) {   // 9
            balance -= amount;
        }
    }

    public double getBalance(){
        return balance;
    }
}


/*
Given the following BankAccount class definition, which of the following statements are
true? (Select two.)
+ A. The lock being acquired on line 4 is for the this reference.
B. A thread must have the appropriate monitor lock before invoking the deposit method.
C. Line 4 generates a compiler error
+ D. The getBalance method can be invoked even if the object ’ s monitor is owned by
another thread.
E. Line 9 generates a compiler error.

9. A and D. The code compiles fine, so C and E are incorrect. Synchronized methods attempt to
acquire the lock on the this reference, so A is true. B is false; a thread can invoke deposit
without the lock. If the thread does not have the lock, it will have to acquire it. D is true;
the getBalance method is not synchronized, so it is possible to invoke getBalance while
another thread is in the middle of a deposit or withdraw. Therefore, the answers are A and D.

 */