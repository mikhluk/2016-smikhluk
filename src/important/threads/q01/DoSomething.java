package important.threads.q01;

/**
 * @author Sergey Mikhluk.
 */
public class DoSomething implements Runnable{
    @Override
    public void run() {
        System.out.println("Do somthing");
    }

    public static void main(String[] args) {
        DoSomething r = new DoSomething();
        Thread t = new Thread(r);
        System.out.println(t.getState());   //NEW
    }

}


/*
1. C. The thread t is instantiated but has not been started yet. That is the definition of a
new thread, so its state is NEW and the answer is C.
 */