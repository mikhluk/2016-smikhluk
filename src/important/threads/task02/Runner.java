package important.threads.task02;

/**
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static void main(String[] args) {
        Thread t1 = new Thread();
        t1.setPriority(7);

        ThreadGroup tg = new ThreadGroup("TG");
        tg.setMaxPriority(4);

        Thread t2 = new Thread(tg,"t2");
        System.out.println("приоритет t1 = "+ t1.getPriority() );  // t1 = 7
        System.out.println("приоритет t2 = "+ t2.getPriority() );   // t2 = 5, если setMaxPriority < 5, то t1 = setMaxPriority

    }
}
