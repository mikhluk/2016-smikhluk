package important.inharitance.examplecourse;

/**
 * @author Sergey Mikhluk.
 */
public class Course {
    public int id = 71;

    public Course(){
        System.out.println("конструктор класса Course");
        System.out.println(" id = " + id);
        id = getId();
        System.out.println(" id = " + getId());
    }

    public int getId() {
        System.out.println("getId() класса Course");
        return id;
    }
}
