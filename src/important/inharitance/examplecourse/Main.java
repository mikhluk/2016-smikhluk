package important.inharitance.examplecourse;

/**
 * @author Sergey Mikhluk.
 */
public class Main {
    public static void main(String[] args) {
        // ms: обратить внимание
        // 1) в конструкторе класса  Course выозовется переопределенный
        // метод getId() класса BaseCourse, в момент когда id еще не будет
        // проинициализированно, получим id = 0;
        Course objA = new BaseCourse();
        BaseCourse objB = new BaseCourse();

        // ms: обратить внимание
        // 2) разные значения id и getId() из-за того что id в классе Course
        // перезатерлось в конктрукторе id = getId();  а в классе BaseCourse
        // оно еще не было проинициализировано инициализация происходит после
        // выполнения super();
        System.out.println("objA. id = " + objA.id);   //id  = 0
        System.out.println("objA. id = " + objA.getId());   //id  = 90
        System.out.println("objB. id = " + objB.id);    //id

        Course objC = new Course();
    }
}
