package important.inharitance.examplecourse;

/**
 * @author Sergey Mikhluk.
 */
public class BaseCourse extends Course {
    public int id = 90;

    public BaseCourse(){
        System.out.println("конструктор класса BaseCourse");
        System.out.println(" id = " + getId());
        id = getId();
    }

    public int getId() {
        System.out.println("getId() класса BaseCourse");
        //System.out.println("getId() super класса BaseCourse = " + super.id);
        return id;
    }
}
