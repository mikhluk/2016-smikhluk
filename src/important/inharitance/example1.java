package important.inharitance;

/**
 * @author Sergey Mikhluk.
 */
public class example1 {
    public static void main(String[] args) {
        A a = new B();
        System.out.println(a.i);
        System.out.println(a.getI());

        B b = new B();
        System.out.println(b.i);
        System.out.println(b.getI());

    }
}

class A {
    static int i = 3;
    static int getI() {return i;}
}

class B extends A{
    static int i = 5;
    static int getI() {return i;}
}

//Статические поля и методы виртуальными не являются, поэтому оба вызова выведут нам 3.