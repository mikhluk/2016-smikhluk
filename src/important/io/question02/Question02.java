package important.io.question02;

/**
 * @author Sergey Mikhluk.
 */
public class Question02 {
    public static void doSomething(int i) {
        System.out.println("method 1");
    }
    public static void doSomething(Byte b) {
        System.out.println("method 2");
    }
    public static void main(String[] args) {
        byte b = -12;
        doSomething(b);  // answer A."method 1", byte прикастовалось к int, а не к Byte
    }
}
