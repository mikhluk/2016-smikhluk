package important.io.question05;

import java.io.PrintWriter;

/**
 * @author Sergey Mikhluk.
 */
public class Question05 {
    public static void main(String[] args) {
        PrintWriter pw = new PrintWriter(System.out);

        double d = 2.12345;
        int s = 3;
        boolean b = s>0;

        pw.format("%4.2f%s %d\n",d," is number", s);    // 4 общее кол-во знаков с учетом запятой, 2 кол-во знаков после запятой
        //pw.format("%5.2f",d);    // добавится пробел перед числом d
        pw.close();
    }
}
