package important.io.question01;

/**
 * @author Sergey Mikhluk.
 */
public class Unboxer {
    private Integer x;

    public boolean compare(int y) {
        return x == y;   // Answer. D. Exception in thread "main" java.lang.NullPointerException
    }

    public static void main(String[] args) {
        Unboxer u = new Unboxer();
        if (u.compare(21)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
