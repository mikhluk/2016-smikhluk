package important.string;

/**
 * @author Sergey Mikhluk.
 */
public class RefString {

    private static void changeStr(Integer i) {
        i = new Integer(10);
    }

    public static void main(String[] args) {
        Integer i = new Integer(1);  // immutable класс, по этому i остается неизменным, после вызова changeStr(i);
        // хотя передача параметра в метод по ссылке, аналогично для объекта String, Double, Float и т.д.

        changeStr(i);
        System.out.println(i);
    }
}
