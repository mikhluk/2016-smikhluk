package com.epam.challenge.task13;

/**
 * @author Sergey Mikhluk.
 */
public class List {
    public static final int CAPACITY = 10;
    private Node head;
    //private Node nodeCurrent;
    private int size;

    public static class Node {
        private int value;
        public Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }
    }

    public List() {
        //this.head = node;
        //this.nodeCurrent = node;
    }

    public void addByValue(int value) {

        size++;
        if (head == null) {
            head = new Node(value, null);
            return;
        }

        Node node = head;

        while (node.next != null) {
            node = node.next;
        }
        node.next = new Node(value, null);
    }

    public void addByNode(Node node) {
        if (node == null) {
            throw new NullPointerException();
        }
        size++;

        if (head == null) {
            head = node;
            return;
        }

        Node nodeTemp = head;

        while (nodeTemp.next != null) {
            nodeTemp = nodeTemp.next;
        }
        nodeTemp.next = node;
    }

    public void findLoop() {
        Node nodeFast = head;
        Node nodeSlow = head;

        int i = 0;
        while (nodeSlow.next != null) {
            nodeSlow = nodeSlow.next;
            if (nodeFast.next != null) {
                nodeFast = nodeFast.next;
            }

            if (nodeFast.next != null) {
                nodeFast = nodeFast.next;
            }
            if (nodeFast == nodeSlow) {
                System.out.println("LOOP!!!");
            }
            System.out.print("Slow value = " + nodeSlow.getValue());
            System.out.println(", Fast value = " + nodeFast.getValue());

            if (i++ >= size * 2) {
                break;
            }
        }
    }


    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(CAPACITY);
        buffer.append("List{");
        Node node = head;
        for (int i = 0; i < size * 2; i++) {   // MS: метод вывода сделан специально до конечного значения, иначе если есть цикл то будем выводить вечно
            buffer.append(node.getValue());
            buffer.append(", ");
            if (node.next == null) {
                break;
            }
            node = node.next;
        }
        buffer.append("}");
        return buffer.toString();
    }

}
