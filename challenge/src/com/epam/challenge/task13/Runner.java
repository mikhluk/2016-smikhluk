package com.epam.challenge.task13;

/**
 * 13. Given a circular linked list, implement an algorithm
 * which returns node at the beginning of the loop.
 * DEFINITIONCircular linked list:
 * A (corrupt) linked list in which a node’s next pointer
 * points to an earlier node, so as to make a loop in the
 * linked list.
 * EXAMPLE
 * input: A -> B -> C -> D -> E -> C [the same C as earlier]
 * output: C
 *
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static void main(String[] args) {
        List circularList = demoCircularList();
        circularList.findLoop();
        //demoList2();
    }


    private static List demoCircularList() {
        List circularList = new List();
        circularList.addByNode(new List.Node(1, null));
        circularList.addByNode(new List.Node(2, null));
        List.Node node = new List.Node(3, null);
        circularList.addByNode(node);
        circularList.addByNode(new List.Node(4, null));
        circularList.addByNode(new List.Node(5, null));
        circularList.addByNode(node);
        System.out.println(circularList);
        return circularList;
    }

    private static void demoList2() {
        List list = new List();
        list.addByValue(1);
        list.addByValue(2);
        list.addByValue(3);
        list.addByValue(4);
        list.addByValue(5);

        System.out.println(list);
    }


}
