13. Given a circular linked list, implement an algorithm
which returns node at the beginning of the loop.
DEFINITIONCircular linked list:
A (corrupt) linked list in which a node’s next pointer
points to an earlier node, so as to make a loop in the
linked list.

EXAMPLE
input: A -> B -> C -> D -> E -> C [the same C as earlier]
output: C


38. A bunch of men are on an island. A genie comes down and
gathers everyone together and places a magical hat on
some people’s heads (i.e., at least one person has a hat).
The hat is magical: it can be seen by other people, but not
by the wearer of the hat himself.To remove the hat, those
(and only those who have a hat) must dunk themselves
underwater at exactly midnight.If there are n people and
c hats, how long does it take the men to remove the hats?

The men cannot tell each other (in any way) that they have
a hat.
FOLLOW UP
Prove that your solution is correct.



63. Given a sorted array of strings which is interspersed
with empty strings, write a method to find the location of
a given string.

Example: find “ball” in
[“at”, “”, “”, “”, “ball”, “”, “”, “car”, “”, “”, “dad”, “”, “”]
will return 4
Example: find “ballcar” in
[“at”, “”, “”, “”, “”, “ball”, “car”, “”, “”, “dad”, “”, “”]
will return -1



10. Implement an algorithm to find the n-th to last element
of a singly linked list.

11. Implement an algorithm to delete a node in the middle of
a single linked list, given only access to that node.

EXAMPLE
Input: the node ‘c’ from the linked list a->b->c->d->e
Result: nothing is returned, but the new linked list looks
like a->b->d->e

12 You have two numbers represented by a linked list, where
each node contains a single digit.The digits are stored in
reverse order, such that the 1’s digit is at the head of the
list.Write a function that adds the two numbers and returns
the sum as a linked list.

EXAMPLE Input: (3 -> 1 -> 5) + (5 -> 9 -> 2)
Output: 8 -> 0 -> 8