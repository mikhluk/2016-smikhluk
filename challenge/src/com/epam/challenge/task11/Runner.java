package com.epam.challenge.task11;

/**
 * 11. Implement an algorithm to delete a node in the middle of
 * a single linked list, given only access to that node.
 * <p>
 * EXAMPLE
 * Input: the node ‘c’ from the linked list a->b->c->d->e
 * Result: nothing is returned, but the new linked list looks
 * like a->b->d->e
 *
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static final int SIZE = 5;
    public static final int DELETE_INDEX = 0;

    public static void main(String[] args) {
        demo1();
        demo2();
    }

    /* Expected result something like that:
        Demo 1.
            Befor delete: List{4, 1, 9, 0, 4}
            size of list = 5
            Deleting node with index = 0, value = 4
            After delete: List{1, 9, 0, 4}
            size of list = 4
     */
    private static void demo1() {
        System.out.println("Demo 1.");
        demo(DELETE_INDEX);
    }

    /* Expected result:
        java.lang.IllegalArgumentException: It is prohibited to delete the last node of list.
        at com.epam.challenge.task11.SimpleList.deleteNode(SimpleList.java:83)
    */
    private static void demo2() {
        System.out.println("Demo 2.");
        demo(SIZE - 1);
    }

    private static void demo(int deleteIndex) {
        SimpleList simpleList = SimpleListFactory.createSimpleList(SIZE);
        System.out.println("    Befor delete: " + simpleList);
        System.out.println("    size of list = " + simpleList.length());

        SimpleList.Node node = simpleList.getNode(deleteIndex);
        System.out.println("    Deleting node with index = " + deleteIndex + ", value = " + node.getValue());

        try {
            simpleList.deleteNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("    After delete: " + simpleList);
        System.out.println("    size of list = " + simpleList.length());
    }
}