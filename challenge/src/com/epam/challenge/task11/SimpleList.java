package com.epam.challenge.task11;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleList {
    private Node head;

    public static class Node {
        private int value;
        public Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }

    public SimpleList() {
    }

    public void add(int value) {
        if (head == null) {
            head = new Node(value, null);
            return;
        }

        Node node = head;
        while (node.next != null) {
            node = node.next;
        }
        node.next = new Node(value, null);
    }

    /**
     * just for sample, is not part of the task
     */
    public int getValue(int index) {
        return getNode(index).getValue();
    }

    public Node getNode(int index) {
        if (head == null) {
            throw new NullPointerException();
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        int position = 0;
        Node node = head;
        while (node.next != null) {
            if (position == index) {
                break;
            }
            node = node.next;
            position++;
        }

        if (position < index) {
            throw new IndexOutOfBoundsException();
        }
        return node;
    }

    public void deleteNode(Node node){
        if (node == null) {
            throw new NullPointerException();
        }

        if (node.next != null) {
            node.setValue(node.next.getValue());
            node.next = node.next.next;
        } else {
            throw new IllegalArgumentException("It is prohibited to delete the last node of list.");
        }
    }


    public int length(){
        if (head == null) {
            return 0;
        }

        int position = 0;
        for (Node node = head; node != null; node = node.next) {
            position++;
        }
        return position;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "List{null}";
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append("List{");

        for (Node node = head; node != null; node = node.next) {
            buffer.append(node.getValue());
            buffer.append(", ");
        }

        buffer.delete(buffer.length() - 2, buffer.length());
        buffer.append("}");
        return buffer.toString();
    }
}


