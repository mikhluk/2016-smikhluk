package com.epam.challenge.task11;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleListFactory {
    public static SimpleList createSimpleList(int size) {
        int range = size < 10 ? 10 : size;
        SimpleList simpleList = new SimpleList();
        for (int x = 0; x < size; x++) {
            simpleList.add((int) (Math.random() * range));
        }
    return simpleList;
    }
}
