package com.epam.challenge.task10;

/**
 * 10. Implement an algorithm to find the n-th to last element
 * of a singly linked list.
 *
 * @author Sergey Mikhluk.
 */
public class Runner {
    public static final int SIZE = 12;

    public static void main(String[] args) {
        SimpleList simpleList = SimpleListFactory.createSimpleList(SIZE);
        System.out.println(simpleList);
        System.out.println("size of list = " + simpleList.length());

        demo(simpleList, SIZE - 1); // expected: the value at 0 position
        demo(simpleList, 0); // expected: the value at last position
        demo(simpleList, 1); // expected: the value at 2-th position to last

        //demo(simpleList, SIZE); // expected: Exception in thread "main" java.lang.IndexOutOfBoundsException
    }

    private static void demo(SimpleList simpleList, int index) {
        System.out.println("" + index +"-th element from last = " + simpleList.getValueFromLast(index));
    }
}
