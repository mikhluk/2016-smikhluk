package com.epam.challenge.task10;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleListFactory {
    public static SimpleList createSimpleList(int size) {
        SimpleList simpleList = new SimpleList();
        for (int x = 0; x < size; x++) {
            simpleList.add((int) (Math.random() * size));
        }
    return simpleList;
    }
}
