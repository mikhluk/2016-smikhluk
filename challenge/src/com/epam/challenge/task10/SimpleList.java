package com.epam.challenge.task10;

/**
 * @author Sergey Mikhluk.
 */
public class SimpleList {
    private Node head;

    public static class Node {
        private int value;
        public Node next;

        public Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }

        public int getValue() {
            return value;
        }
    }

    public SimpleList() {
    }

    public void add(int value) {
        if (head == null) {
            head = new Node(value, null);
            return;
        }

        Node node = head;
        while (node.next != null) {
            node = node.next;
        }
        node.next = new Node(value, null);
    }

    /**
     * just for sample, is not part of the task
     */
    public int getValue(int index) {
        if (head == null) {
            throw new NullPointerException();
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        int position = 0;
        Node node = head;
        while (node.next != null) {
            if (position == index) {
                break;
            }
            node = node.next;
            position++;
        }

        if (position < index) {
            throw new IndexOutOfBoundsException();
        }
        return node.getValue();
    }

    /**
     * If the SimpleList object have n elements,
     * then n-th element has index = 0 from last, so
     * getValueFromLast(0) is the same that getValue(n-1)
     * getValueFromLast(n-1) is the same that getValue(0)
     *
     * @param index can be integer number from 0 to list.length()-1
     * @return the value at index-th position to last element
     */
    public int getValueFromLast(int index) {
        if (head == null) {
            throw new NullPointerException();
        }
        if (index < 0) {
            throw new IndexOutOfBoundsException();
        }

        int position = 0;
        Node node1 = head;
        Node node2 = head;

        while (node1.next != null) {
            if (position >= index) {
                node2 = node2.next;
            }
            node1 = node1.next;
            position++;
        }

        if (position < index) {
            throw new IndexOutOfBoundsException();
        }
        return node2.getValue();
    }


    public int length(){
        if (head == null) {
            return 0;
        }

        int position = 0;
        for (Node node = head; node != null; node = node.next) {
            position++;
        }
        return position;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "List{null}";
        }

        StringBuffer buffer = new StringBuffer();
        buffer.append("List{");

        for (Node node = head; node != null; node = node.next) {
            buffer.append(node.getValue());
            buffer.append(", ");
        }

        buffer.delete(buffer.length() - 2, buffer.length());
        buffer.append("}");
        return buffer.toString();
    }
}


